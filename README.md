Api de testes em: https://farmaalg-backend.herokuapp.com

Documentação da Api existente em: https://farmaalg-backend.herokuapp.com/swagger-ui/index.html#/



## Comandos para executar o projeto localmente:

- Adicione as configurações de seu banco postgres em application.properties


- Verifique a versão do maven instalada ou instale no seu sistema:
```
mvn --version
```

- Faça o build do projeto e das dependencias com:
```
mvn package
```

- Altere a permissão de mvnw com:
```
chmod 770 mvnw
```

- Execute o servidor com:
```
./mvnw spring-boot:run
```



## Comandos de CI do Heroku:

```
heroku login
heroku git:remote -a farmaalg-backend
git push heroku master
```