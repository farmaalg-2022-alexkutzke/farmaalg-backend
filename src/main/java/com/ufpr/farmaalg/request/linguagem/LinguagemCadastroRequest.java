package com.ufpr.farmaalg.request.linguagem;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LinguagemCadastroRequest {
    private String nome;
    private String codigoSandbox;
    private String exemplo;
}
