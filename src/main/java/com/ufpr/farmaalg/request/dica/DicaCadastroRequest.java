package com.ufpr.farmaalg.request.dica;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class DicaCadastroRequest {

    private IdRelacionamento exercicio;
    private String titulo;
    private String descricao;
    private int numeroTentativas;
}
