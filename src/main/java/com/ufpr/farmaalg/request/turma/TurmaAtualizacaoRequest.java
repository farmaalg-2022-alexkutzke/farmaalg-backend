package com.ufpr.farmaalg.request.turma;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class TurmaAtualizacaoRequest {

    private Long id;
    private IdRelacionamento professor;
    private String disciplina;
    private String nome;
    private String descricao;
    private String codigoAcesso;
    private String status;
}
