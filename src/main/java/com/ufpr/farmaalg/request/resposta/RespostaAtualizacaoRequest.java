package com.ufpr.farmaalg.request.resposta;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RespostaAtualizacaoRequest {

    private Long id;
    private String codigo;
    private boolean ehFinal;
    private IdRelacionamento aluno;
    private IdRelacionamento exercicio;
    private IdRelacionamento linguagem;
    private int numeroTentativa;
}
