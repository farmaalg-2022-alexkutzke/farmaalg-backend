package com.ufpr.farmaalg.request.resposta;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RespostaCadastroRequest {

    private String codigo;
    private boolean ehFinal;
    private IdRelacionamento aluno;
    private IdRelacionamento exercicio;
    private IdRelacionamento linguagem;
}
