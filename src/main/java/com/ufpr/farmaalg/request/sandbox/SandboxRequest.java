package com.ufpr.farmaalg.request.sandbox;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SandboxRequest {
    private String lang;
    private String code;
    private String[] params;
}
