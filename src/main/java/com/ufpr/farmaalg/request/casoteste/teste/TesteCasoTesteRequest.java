package com.ufpr.farmaalg.request.casoteste.teste;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class TesteCasoTesteRequest {

    private IdRelacionamento linguagem;
    private String codigo;
}
