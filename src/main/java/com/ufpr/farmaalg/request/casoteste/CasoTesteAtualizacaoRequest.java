package com.ufpr.farmaalg.request.casoteste;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class CasoTesteAtualizacaoRequest {

    private Long id;
    private IdRelacionamento exercicio;
    private String titulo;
    private String input;
    private String output;
}
