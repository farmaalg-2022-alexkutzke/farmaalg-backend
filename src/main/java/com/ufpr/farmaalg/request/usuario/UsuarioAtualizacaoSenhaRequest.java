package com.ufpr.farmaalg.request.usuario;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class UsuarioAtualizacaoSenhaRequest {

    private String senhaAntiga;
    private String senhaNova;
}
