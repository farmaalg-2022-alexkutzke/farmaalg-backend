package com.ufpr.farmaalg.request.usuario;

import lombok.Getter;
import lombok.Setter;


/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class AdminAtualizacaoSenhaRequest {

    private Long id;
    private String senhaNova;
}
