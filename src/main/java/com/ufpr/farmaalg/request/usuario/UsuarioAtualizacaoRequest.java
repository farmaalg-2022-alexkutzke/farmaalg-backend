package com.ufpr.farmaalg.request.usuario;

import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;


/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class UsuarioAtualizacaoRequest {

    private Long id;

    @Enumerated(EnumType.STRING)
    private Perfil perfil;

    private IdRelacionamento instituicao;
    private String nome;
    private String email;
    private String idInstitucional;
    private String telefone;
    private String avatar;
}
