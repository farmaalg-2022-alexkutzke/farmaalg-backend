package com.ufpr.farmaalg.request.listaexercicio;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;


/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class ListaExercicioCadastroRequest {

    private String titulo;
    private String descricao;
    private LocalDateTime dataEntrega;
    private IdRelacionamento turma;
    private List<IdRelacionamento> exercicios;
}
