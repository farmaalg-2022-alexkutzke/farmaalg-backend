package com.ufpr.farmaalg.request.exercicio;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class DicaRelacionamentoRequest {
    private String titulo;
    private String descricao;
    private int numeroTentativas;
}
