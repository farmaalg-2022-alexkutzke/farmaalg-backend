package com.ufpr.farmaalg.request.exercicio;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class ExercicioCadastroRequest {

    private String titulo;
    private String descricao;
    private String enunciado;
    private boolean ehPublico;
    private IdRelacionamento turma;
}
