package com.ufpr.farmaalg.request.exercicio;

import com.ufpr.farmaalg.request.IdRelacionamento;
import com.ufpr.farmaalg.request.tag.TagCadastroRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class ExercicioAtualizacaoRequest {

    private Long id;
    private String titulo;
    private String descricao;
    private String enunciado;
    private boolean ehPublico;
    private IdRelacionamento turma;
    private List<DicaRelacionamentoRequest> dicas;
    private List<TagCadastroRequest> tags;
    private List<IdRelacionamento> linguagens;
}
