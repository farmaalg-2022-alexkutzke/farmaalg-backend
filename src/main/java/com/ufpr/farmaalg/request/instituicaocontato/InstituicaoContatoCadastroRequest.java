package com.ufpr.farmaalg.request.instituicaocontato;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class InstituicaoContatoCadastroRequest {
    private String contato;
    private String tipo;
    private String pessoa;
    private IdRelacionamento instituicao;
}
