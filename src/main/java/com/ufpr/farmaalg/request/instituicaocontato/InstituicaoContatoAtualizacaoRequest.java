package com.ufpr.farmaalg.request.instituicaocontato;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class InstituicaoContatoAtualizacaoRequest {
    private Long id;
    private String contato;
    private String tipo;
    private String pessoa;
}
