package com.ufpr.farmaalg.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IdRelacionamento {
    private Long id;
}
