package com.ufpr.farmaalg.request.instituicao;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class InstituicaoCadastroRequest {
    private String nome;
    private String cnpj;
    private String endereco;
    private String numero;
    private String bairro;
    private String cep;
    private String cidade;
    private String estado;
    private String avatar;
}
