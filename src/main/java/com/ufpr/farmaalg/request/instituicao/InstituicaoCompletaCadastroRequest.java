package com.ufpr.farmaalg.request.instituicao;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class InstituicaoCompletaCadastroRequest {
    private String nome;
    private String cnpj;
    private String endereco;
    private String numero;
    private String bairro;
    private String cep;
    private String cidade;
    private String estado;
    private String avatar;
    private List<InstituicaoCompletaContatoCadastroRequest> contatos;
}
