package com.ufpr.farmaalg.request.instituicao;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
public class InstituicaoCompletaContatoCadastroRequest {
    private String contato;
    private String tipo;
    private String pessoa;
}
