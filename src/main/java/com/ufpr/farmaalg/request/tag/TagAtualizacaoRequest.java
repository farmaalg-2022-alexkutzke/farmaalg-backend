package com.ufpr.farmaalg.request.tag;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TagAtualizacaoRequest {
    private Long id;
    private String nome;
}
