package com.ufpr.farmaalg.config.security;

import static com.ufpr.farmaalg.config.security.util.SecurityConstants.*;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ufpr.farmaalg.config.security.service.UserService;
import com.ufpr.farmaalg.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @author Lia Alflen
 */

@AllArgsConstructor
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;

    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        Usuario user;

        try {
            user = new ObjectMapper()
                    .readValue(request.getInputStream(), Usuario.class);

        } catch (Exception e) {
            setUserMessageOnReturn(response, 400, "error","Could not find username and password on JSON");
            return null;
        }

        return this.authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), salt+user.getSenha()));
    }

    private void setUserMessageOnReturn(HttpServletResponse response, int statusCode, String key, String message) throws IOException {
        response.setStatus(statusCode);

        Map<String, String> body = new HashMap<>();
        body.put(key, message);
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(), body);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        User user = (User) authResult.getPrincipal();
        Algorithm algorithm = Algorithm.HMAC256(secret.getBytes());
        String access_token = JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .withIssuer(request.getRequestURL().toString())
                .withClaim("id", userService.getIdByUsername(user.getUsername()))
                .withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(algorithm);

        setUserMessageOnReturn(response, 200, "token",access_token);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        System.out.println(failed.toString());
        setUserMessageOnReturn(response, 400, "error", failed.getMessage());
    }

    public static Long getIdFromToken(String token) {
        return JWT.decode(token.substring(PREFIX.length())).getClaim("id").asLong();
    }

    public static String getUsernameFromToken(String token) {
        return JWT.decode(token.substring(PREFIX.length())).getSubject();
    }

    public static String getRoleFromToken(String token) {
        return JWT.decode(token.substring(PREFIX.length())).getClaim("roles").asList(String.class).get(0).substring("ROLE_".length());
    }
}
