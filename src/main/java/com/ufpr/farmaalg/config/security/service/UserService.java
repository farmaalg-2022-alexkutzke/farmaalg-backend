package com.ufpr.farmaalg.config.security.service;

import com.ufpr.farmaalg.model.Usuario;
import com.ufpr.farmaalg.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lia Alflen
 */

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuario usuario = getUsuarioByEmail(email);

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();

        authorities.add(new SimpleGrantedAuthority("ROLE_" + usuario.getPerfil().name()));

        return new User(usuario.getEmail(), usuario.getSenha(), authorities);
    }

    public Long getIdByUsername(String email) {
        Usuario usuario = getUsuarioByEmail(email);

        return usuario.getId();
    }

    private Usuario getUsuarioByEmail(String email) {
        Usuario usuario = usuarioRepository.findByEmail(email);

        if (usuario == null) {
            throw new UsernameNotFoundException("User not found!");
        }
        return usuario;
    }


}
