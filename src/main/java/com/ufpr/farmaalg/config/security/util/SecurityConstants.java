package com.ufpr.farmaalg.config.security.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Lia Alflen
 */

@Component
public class SecurityConstants {

     public final static String PREFIX = "Bearer ";
     public final static String HEADER = "Authorization";
     public final static Long EXPIRATION_TIME = 43200000L ; //12horas //600000L 10 minutos
     public static String salt;
     public static String secret;

    @Value("${farmaalg.security.salt}")
    public void setSalt(String salt) {
        SecurityConstants.salt = salt;
    }

    @Value("${farmaalg.security.secret}")
    public void setSecret(String secret) {
        SecurityConstants.secret = secret;
    }
}
