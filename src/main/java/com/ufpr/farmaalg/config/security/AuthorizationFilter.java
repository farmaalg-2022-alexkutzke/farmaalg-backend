package com.ufpr.farmaalg.config.security;

import static com.ufpr.farmaalg.config.security.util.SecurityConstants.*;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lia Alflen
 */

public class AuthorizationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        //se estiver acessando pelo login, passa para o filter de authentication
        if (request.getServletPath().equals("/login")) {
            filterChain.doFilter(request, response);
        } else {
            String token = request.getHeader(HEADER);

            if (token != null && (!token.isEmpty()) && token.startsWith(PREFIX)) {
                try {
                    token = token.substring(PREFIX.length());

                    Algorithm algorithm = Algorithm.HMAC256(secret.getBytes());

                    JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                    DecodedJWT decodedToken = jwtVerifier.verify(token);

                    String username = decodedToken.getSubject();
                    List<String> roles = decodedToken.getClaim("roles").asList(String.class);

                    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
                    roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));

                    var authToken = new UsernamePasswordAuthenticationToken(username, null, authorities);
                    SecurityContextHolder.getContext().setAuthentication(authToken);

                    filterChain.doFilter(request, response);
                } catch (Exception e) {
                    e.printStackTrace();
                    response.setStatus(403);
                }
            } else {
                filterChain.doFilter(request, response);
            }
        }
    }
}
