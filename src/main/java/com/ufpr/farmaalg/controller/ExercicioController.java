package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.exercicio.ExercicioAtualizacaoRequest;
import com.ufpr.farmaalg.request.exercicio.ExercicioCadastroRequest;
import com.ufpr.farmaalg.request.exercicio.ExercicioCompletoCadastroRequest;
import com.ufpr.farmaalg.response.exercicio.ExercicioResponse;
import com.ufpr.farmaalg.service.ExercicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/exercicio")
public class ExercicioController {

    @Autowired
    private ExercicioService exercicioService;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> ListarExerciciosPublicos(@RequestHeader("Authorization") String token) {
        List<ExercicioResponse> exercicios = exercicioService.buscarTodosExerciciosPublicos(token);

        if (exercicios == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(exercicios);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> buscarExercicio(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        ExercicioResponse exercicio = exercicioService.buscarExercicioPorCodigo(codigo, token);

        if (exercicio == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(exercicio);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarExercicio(@RequestBody ExercicioCadastroRequest exercicio, @RequestHeader("Authorization") String token) {
        ExercicioResponse exercicioSalvo = exercicioService.cadastrarNovoExercicio(exercicio, token);

        if (exercicioSalvo == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(exercicioSalvo);
    }

    @PostMapping("/completo")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarExercicioCompleto(@RequestBody ExercicioCompletoCadastroRequest exercicio, @RequestHeader("Authorization") String token) {
        try {
            ExercicioResponse exercicioSalvo = exercicioService.cadastrarNovoExercicioCompleto(exercicio, token);

            return ResponseEntity.status(HttpStatus.CREATED).body(exercicioSalvo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }
    }

    @PostMapping("/{codigoExercicio}/turma/{codigoTurma}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarExercicioParaOutraTurma(@PathVariable Long codigoExercicio, @PathVariable Long codigoTurma) {
        try {
            ExercicioResponse exercicioSalvo = exercicioService.cadastraExercicioParaOutroProfessor(codigoExercicio, codigoTurma);

            return ResponseEntity.status(HttpStatus.CREATED).body(exercicioSalvo);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> atualizarExercicio(@PathVariable Long codigo, @RequestBody ExercicioAtualizacaoRequest exercicio, @RequestHeader("Authorization") String token) {
        try {
            ExercicioResponse exercicioAtualizado = exercicioService.atualizarExercicio(codigo, exercicio, token);

            return ResponseEntity.ok(exercicioAtualizado);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> removerExercicio(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        boolean isremoved = exercicioService.removerExercicio(codigo, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }

    @PostMapping("/{codigoExercicio}/lista/{codigoLista}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarExercicioEmUmaLista(@PathVariable Long codigoExercicio, @PathVariable Long codigoLista, @RequestHeader("Authorization") String token) {
        boolean isCadastrated = exercicioService.adicionarExercicioAUmaLista(codigoExercicio, codigoLista, token);

        if (!isCadastrated) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Exercicio cadastrado na lista.");
    }

    @DeleteMapping("/{codigoExercicio}/lista/{codigoLista}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> removerExercicioDeUmaLista(@PathVariable Long codigoExercicio, @PathVariable Long codigoLista, @RequestHeader("Authorization") String token) {
        boolean isremoved = exercicioService.removerExercicioDeUmaLista(codigoExercicio, codigoLista, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }

    @PostMapping("/{codigoExercicio}/tag/{nomeTag}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarTagEmUmExercicio(@PathVariable Long codigoExercicio, @PathVariable String nomeTag, @RequestHeader("Authorization") String token) {
        boolean isCadastrated = exercicioService.adicionarTagAUmExercicio(codigoExercicio, nomeTag, token);

        if (!isCadastrated) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Tag adicionada no exercício.");
    }

    @DeleteMapping("/{codigoExercicio}/tag/{codigoTag}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> removerTagDeUmExercicio(@PathVariable Long codigoExercicio, @PathVariable Long codigoTag, @RequestHeader("Authorization") String token) {
        boolean isremoved = exercicioService.removerTagDeUmExercicio(codigoExercicio, codigoTag, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }

    //Todo: fazer endpoint de buscar todas as tags de um exercicio

    @PostMapping("/{codigoExercicio}/linguagem/{codigoLinguagem}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarLinguagemEmUmExercicio(@PathVariable Long codigoExercicio, @PathVariable Long codigoLinguagem, @RequestHeader("Authorization") String token) {
        boolean isCadastrated = exercicioService.adicionarLinguagemAUmExercicio(codigoExercicio, codigoLinguagem, token);

        if (!isCadastrated) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Linguagem adicionada no exercício.");
    }

    @DeleteMapping("/{codigoExercicio}/linguagem/{codigoLinguagem}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> removerLinguagemDeUmExercicio(@PathVariable Long codigoExercicio, @PathVariable Long codigoLinguagem, @RequestHeader("Authorization") String token) {
        boolean isremoved = exercicioService.removerLinguagemDeUmExercicio(codigoExercicio, codigoLinguagem, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
