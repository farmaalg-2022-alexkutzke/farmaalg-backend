package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.casoteste.CasoTesteAtualizacaoRequest;
import com.ufpr.farmaalg.request.casoteste.CasoTesteCadastroRequest;
import com.ufpr.farmaalg.request.casoteste.teste.TesteCasoTesteRequest;
import com.ufpr.farmaalg.response.casoteste.CasoTesteCompletoResponse;
import com.ufpr.farmaalg.response.casoteste.CasoTesteResponse;
import com.ufpr.farmaalg.response.resposta.CasoTesteRespostaResponse;
import com.ufpr.farmaalg.service.CasoTesteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/caso/teste")
public class CasoTesteController {

    @Autowired
    private CasoTesteService casoTesteService;

    @GetMapping("exercicio/{codigoExercicio}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> ListarCasosTesteDeUmExercicio(@PathVariable Long codigoExercicio, @RequestHeader("Authorization") String token) {
        List<CasoTesteCompletoResponse> casoTesteResponse = casoTesteService.buscarTodosCasosTesteDeUmExercicio(codigoExercicio, token);

        if (casoTesteResponse == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(casoTesteResponse);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> buscarCasoTeste(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        CasoTesteResponse casoTesteResponse = casoTesteService.buscarCasoTestePorCodigo(codigo, token);

        if (casoTesteResponse == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(casoTesteResponse);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarCasoTeste(@RequestBody CasoTesteCadastroRequest casoTeste, @RequestHeader("Authorization") String token) {
        CasoTesteResponse casoTesteSalvo = casoTesteService.cadastrarNovoCasoTeste(casoTeste, token);

        if (casoTesteSalvo == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(casoTesteSalvo);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> atualizarCasoTeste(@PathVariable Long codigo, @RequestBody CasoTesteAtualizacaoRequest casoTeste, @RequestHeader("Authorization") String token) {
        CasoTesteResponse casoTesteAtualizado = casoTesteService.atualizarCasoTeste(codigo, casoTeste, token);

        if (casoTesteAtualizado == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(casoTesteAtualizado);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> removerCasoTeste(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        boolean isremoved = casoTesteService.removerCasoTeste(codigo, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }

    @PostMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> testarCasoTeste(@RequestBody TesteCasoTesteRequest testeCasoTesteRequest, @PathVariable Long codigo, @RequestHeader("Authorization") String token) throws InterruptedException, IOException, URISyntaxException {
        CasoTesteRespostaResponse casoTesteRespostaResponse = casoTesteService.testarCasoTeste(testeCasoTesteRequest, codigo, token);

        if (casoTesteRespostaResponse == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.status(HttpStatus.OK).body(casoTesteRespostaResponse);
    }

//    @PostMapping("/{codigoCasoTeste}/resposta/{codigoResposta}")
//    public ResponseEntity<?> cadastrarCasoDeTesteEmUmaResposta(@PathVariable Long codigoCasoTeste, @PathVariable Long codigoResposta) {
//        //Todo: chamar sandbox para validar o caso de teste
//        boolean isCadastrated = casoTesteService.adicionarCasoDeTesteAUmaResposta(codigoCasoTeste, codigoResposta);
//
//        if (!isCadastrated) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
//        }
//
//        return ResponseEntity.ok("Caso de teste adicionado na resposta.");
//    }
//
//    @DeleteMapping("/{codigoCasoTeste}/resposta/{codigoResposta}")
//    public ResponseEntity<?> removerCasoDeTesteDeUmaResposta(@PathVariable Long codigoCasoTeste, @PathVariable Long codigoResposta) {
//        boolean isremoved = casoTesteService.removerCasoDeTesteDeUmaResposta(codigoCasoTeste, codigoResposta);
//
//        if (!isremoved) {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
//        }
//
//        return ResponseEntity.ok("Recurso removido.");
//    }
}
