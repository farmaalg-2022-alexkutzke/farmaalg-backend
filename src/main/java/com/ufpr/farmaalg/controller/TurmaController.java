package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.turma.TurmaAtualizacaoRequest;
import com.ufpr.farmaalg.request.turma.CodigoAcessoRequest;
import com.ufpr.farmaalg.request.turma.TurmaCadastroRequest;
import com.ufpr.farmaalg.response.turma.TurmaCompletaResponse;
import com.ufpr.farmaalg.response.turma.TurmaResponse;
import com.ufpr.farmaalg.service.TurmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/turma")
public class TurmaController {

    @Autowired
    private TurmaService turmaService;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> ListarTurmas(@RequestHeader("Authorization") String token) {
        List<TurmaResponse> turmas = turmaService.buscarTodasTurmas(token);

        return ResponseEntity.ok(turmas);
    }

    //Todo: Fazer endpoint para ver turmas ativas: admin pode ver todas, professor vê só as dele e aluno vê apenas as da sua instituição

    @GetMapping("/mim")
    @PreAuthorize("hasRole('ALUNO')")
    public ResponseEntity<?> buscarTurmasDoAluno(@RequestHeader("Authorization") String token) {
        List<TurmaResponse> turmas = turmaService.buscarTurmasDoAlunoPorToken(token);

        return ResponseEntity.ok(turmas);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> buscarTurmaPorCodigo(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        TurmaCompletaResponse turma = turmaService.buscarTurmaPorCodigo(codigo, token);

        if (turma == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(turma);
    }

    @GetMapping("/codigo/acesso/{codigoAcesso}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> consultarDisponibilidadeCodigoAcesso(@PathVariable String codigoAcesso) {
        boolean estahDisponivel = turmaService.consultarDisponibilidadeCodigoAcesso(codigoAcesso);

        if (!estahDisponivel) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Código de acesso não disponível");
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarTurma(@RequestBody TurmaCadastroRequest turma, @RequestHeader("Authorization") String token) {
        TurmaResponse turmaSalva = turmaService.cadastrarNovaTurma(turma, token);

        if (turmaSalva == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(turmaSalva);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> atualizarTurma(@PathVariable Long codigo, @RequestBody TurmaAtualizacaoRequest turma, @RequestHeader("Authorization") String token) {
        TurmaResponse turmaAtualizado = turmaService.atualizarTurma(codigo, turma, token);

        if (turmaAtualizado == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(turmaAtualizado);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> removerTurma(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        boolean isRemoved = turmaService.removerTurma(codigo, token);

        if (!isRemoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }

    @PostMapping("/aluno/{codigoAluno}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('ALUNO')")
    public ResponseEntity<?> cadastrarAlunoTurma(@RequestBody CodigoAcessoRequest codigoAcessoRequest, @PathVariable Long codigoAluno, @RequestHeader("Authorization") String token) {
        TurmaResponse turma = turmaService.cadastrarAlunoTurma(codigoAcessoRequest, codigoAluno, token);

        if (turma == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(turma);
    }

    @DeleteMapping("/{codigoTurma}/aluno/{codigoAluno}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('ALUNO')")
    public ResponseEntity<?> removerAlunoTurma(@PathVariable Long codigoTurma, @PathVariable Long codigoAluno, @RequestHeader("Authorization") String token) {
        boolean isRemoved = turmaService.removerAlunoTurma(codigoTurma, codigoAluno, token);

        if (!isRemoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Aluno removido da turma.");
    }
}
