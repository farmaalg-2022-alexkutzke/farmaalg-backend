package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.resposta.RespostaAtualizacaoRequest;
import com.ufpr.farmaalg.request.resposta.RespostaCadastroRequest;
import com.ufpr.farmaalg.response.resposta.RespostaResponse;
import com.ufpr.farmaalg.service.RespostaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/resposta")
public class RespostaController {

    @Autowired
    private RespostaService respostaService;

    @GetMapping("/exercicio/{codigoExercicio}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> ListarRespostasDeUmExercicio(@PathVariable Long codigoExercicio, @RequestHeader("Authorization") String token) {
        List<RespostaResponse> respostas = respostaService.buscarTodasRespostasDeUmExercicio(codigoExercicio, token);

        if (respostas == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(respostas);
    }

    @GetMapping("/aluno/{codigoAluno}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> ListarRespostasDeUmAluno(@PathVariable Long codigoAluno, @RequestHeader("Authorization") String token) {
        List<RespostaResponse> respostas = respostaService.buscarTodasRespostasDeUmAluno(codigoAluno, token);

        if (respostas == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(respostas);
    }

    @GetMapping("/aluno/{codigoAluno}/exercicio/{codigoExercicio}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> ListarRespostasDeUmAlunoAUmExercicio(@PathVariable Long codigoAluno, @PathVariable Long codigoExercicio, @RequestHeader("Authorization") String token) {
        List<RespostaResponse> respostas = respostaService.buscarRespostasDeUmAlunoEmUmExercicio(codigoAluno, codigoExercicio, token);

        if (respostas == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(respostas);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> buscarResposta(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        RespostaResponse respostaResponse = respostaService.buscarRespostaPorCodigo(codigo, token);

        if (respostaResponse == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(respostaResponse);
    }

    @PostMapping
    @PreAuthorize("hasRole('ALUNO')")
    public ResponseEntity<?> cadastrarResposta(@RequestBody RespostaCadastroRequest resposta, @RequestHeader("Authorization") String token) throws InterruptedException, IOException, URISyntaxException {
        RespostaResponse respostaSalva = respostaService.cadastrarNovaResposta(resposta, token);

        if (respostaSalva == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(respostaSalva);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ALUNO')")
    public ResponseEntity<?> atualizarResposta(@PathVariable Long codigo, @RequestBody RespostaAtualizacaoRequest resposta, @RequestHeader("Authorization") String token) {
        RespostaResponse respostaAtualizada = respostaService.atualizarResposta(codigo, resposta, token);

        if (respostaAtualizada == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(respostaAtualizada);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('ALUNO')")
    public ResponseEntity<?> removerResposta(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        boolean isremoved = respostaService.removerResposta(codigo, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
