package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.usuario.AdminAtualizacaoSenhaRequest;
import com.ufpr.farmaalg.request.usuario.UsuarioAtualizacaoRequest;
import com.ufpr.farmaalg.request.usuario.UsuarioAtualizacaoSenhaRequest;
import com.ufpr.farmaalg.request.usuario.UsuarioCadastroRequest;
import com.ufpr.farmaalg.response.usuario.UsuarioResponse;
import com.ufpr.farmaalg.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> ListarUsuarios(@RequestHeader("Authorization") String token) {
        List<UsuarioResponse> usuarios = usuarioService.buscarTodosUsuarios(token);

        return ResponseEntity.ok(usuarios);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> buscarUsuario(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        UsuarioResponse usuario = usuarioService.buscarUsuarioPorCodigo(codigo, token);

        return ResponseEntity.ok(usuario);
    }

    @GetMapping("/mim")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> buscarUsuarioPorToken(@RequestHeader("Authorization") String token) {
        UsuarioResponse usuario = usuarioService.buscarUsuarioPorToken(token);

        if (usuario == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(usuario);
    }

    @PostMapping
    public ResponseEntity<?> cadastrarUsuario(@RequestBody UsuarioCadastroRequest usuario) {
        UsuarioResponse usuarioSalvo = usuarioService.cadastrarNovoUsuario(usuario);

        if (usuarioSalvo == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email já cadastrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> atualizarUsuario(@PathVariable Long codigo, @RequestBody UsuarioAtualizacaoRequest usuario, @RequestHeader("Authorization") String token) {
        UsuarioResponse usuarioAtualizado = usuarioService.atualizarUsuario(codigo, usuario, token);

        if (usuarioAtualizado == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(usuarioAtualizado);
    }

    @PutMapping("/senha")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> atualizarSenhaUsuario(@RequestBody UsuarioAtualizacaoSenhaRequest usuario, @RequestHeader("Authorization") String token) {
        UsuarioResponse usuarioAtualizado = usuarioService.atualizarSenhaUsuario(usuario, token);

        if (usuarioAtualizado == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(usuarioAtualizado);
    }

    @PutMapping("/admin/senha")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> adminAtualizarSenhaUsuario(@RequestBody AdminAtualizacaoSenhaRequest usuario) {
        UsuarioResponse usuarioAtualizado = usuarioService.adminAtualizarSenhaUsuario(usuario);

        if (usuarioAtualizado == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(usuarioAtualizado);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR') || hasRole('ALUNO')")
    public ResponseEntity<?> removerUsuario(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        boolean isremoved = usuarioService.removerUsuario(codigo, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
