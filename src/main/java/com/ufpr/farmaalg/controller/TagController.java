package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.tag.TagAtualizacaoRequest;
import com.ufpr.farmaalg.request.tag.TagCadastroRequest;
import com.ufpr.farmaalg.response.tag.TagComExerciciosResponse;
import com.ufpr.farmaalg.response.tag.TagResponse;
import com.ufpr.farmaalg.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> ListarTags() {
        List<TagResponse> tags = tagService.buscarTodasTags();

        return ResponseEntity.ok(tags);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> buscarTag(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        TagComExerciciosResponse tag = tagService.buscarTagPorCodigo(codigo, token);

        if (tag == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(tag);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarTag(@RequestBody TagCadastroRequest tag) {
        TagResponse tagSalva = tagService.cadastrarNovaTag(tag);

        return ResponseEntity.status(HttpStatus.CREATED).body(tagSalva);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> atualizarTag(@PathVariable Long codigo, @RequestBody TagAtualizacaoRequest tag) {
        TagResponse tagAtualizada = tagService.atualizarTag(codigo, tag);

        if (tagAtualizada == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(tagAtualizada);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> removerTag(@PathVariable Long codigo) {
        boolean isremoved = tagService.removerTag(codigo);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
