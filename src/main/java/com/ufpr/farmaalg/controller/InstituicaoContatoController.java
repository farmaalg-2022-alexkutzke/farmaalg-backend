package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.instituicaocontato.InstituicaoContatoAtualizacaoRequest;
import com.ufpr.farmaalg.request.instituicaocontato.InstituicaoContatoCadastroRequest;
import com.ufpr.farmaalg.response.instituicaocontato.InstituicaoContatoResponse;
import com.ufpr.farmaalg.service.InstituicaoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/instituicao/contato")
public class InstituicaoContatoController {

    @Autowired
    private InstituicaoContatoService instituicaoContatoService;

    //Todo: listar contato de apenas uma instituição.
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> ListarInstituicoesContatos() {
        List<InstituicaoContatoResponse> instituicoesContatos = instituicaoContatoService.buscarTodasInstituicoesContatos();

        return ResponseEntity.ok(instituicoesContatos);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> buscarInstituicaoContato(@PathVariable Long codigo) {
        InstituicaoContatoResponse instituicaoContato = instituicaoContatoService.buscarInstituicaoContatoPorCodigo(codigo);

        return ResponseEntity.ok(instituicaoContato);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> cadastrarInstituicaoContato(@RequestBody InstituicaoContatoCadastroRequest instituicaoContato) {
        InstituicaoContatoResponse instituicaoContatoSalvo = instituicaoContatoService.cadastrarNovaInstituicaoContato(instituicaoContato);

        if (instituicaoContatoSalvo == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(instituicaoContatoSalvo);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> atualizarInstituicaoContato(@PathVariable Long codigo, @RequestBody InstituicaoContatoAtualizacaoRequest instituicaoContato) {
        InstituicaoContatoResponse instituicaoContatoAtualizado = instituicaoContatoService.atualizarInstituicaoContato(codigo, instituicaoContato);

        if (instituicaoContatoAtualizado == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(instituicaoContatoAtualizado);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> removerInstituicaoContato(@PathVariable Long codigo) {
        boolean isremoved = instituicaoContatoService.removerInstituicaoContato(codigo);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
