package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.instituicao.InstituicaoAtualizacaoRequest;
import com.ufpr.farmaalg.request.instituicao.InstituicaoCadastroRequest;
import com.ufpr.farmaalg.request.instituicao.InstituicaoCompletaCadastroRequest;
import com.ufpr.farmaalg.response.instituicao.InstituicaoComboResponse;
import com.ufpr.farmaalg.response.instituicao.InstituicaoResponse;
import com.ufpr.farmaalg.service.InstituicaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/instituicao")
public class InstituicaoController {

    @Autowired
    private InstituicaoService instituicaoService;


    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> ListarInstituicoes() {
        List<InstituicaoResponse> instituicoes = instituicaoService.buscarTodasInstituicoes();

        return ResponseEntity.ok(instituicoes);
    }

    @GetMapping("/combo")
    public ResponseEntity<?> buscarComboInstituicao() {
        List<InstituicaoComboResponse> instituicoes = instituicaoService.buscarComboInstituicoes();

        return ResponseEntity.ok(instituicoes);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> buscarInstituicao(@PathVariable Long codigo) {
        InstituicaoResponse instituicao = instituicaoService.buscarInstituicaoPorCodigo(codigo);

        return ResponseEntity.ok(instituicao);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> cadastrarInstituicao(@RequestBody InstituicaoCadastroRequest instituicao) {
        InstituicaoResponse instituicaoSalva = instituicaoService.cadastrarNovaInstituicao(instituicao);

        return ResponseEntity.status(HttpStatus.CREATED).body(instituicaoSalva);
    }

    @PostMapping("/completa")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> cadastrarInstituicaoCompleta(@RequestBody InstituicaoCompletaCadastroRequest instituicao) {
        InstituicaoResponse instituicaoSalva = instituicaoService.cadastrarNovaInstituicaoCompleta(instituicao);

        return ResponseEntity.status(HttpStatus.CREATED).body(instituicaoSalva);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> atualizarInstituicao(@PathVariable Long codigo, @RequestBody InstituicaoAtualizacaoRequest instituicao) {
        InstituicaoResponse instituicaoAtualizada = instituicaoService.atualizarInstituicao(codigo, instituicao);

        if (instituicaoAtualizada == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(instituicaoAtualizada);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> removerInstituicao(@PathVariable Long codigo) {
        boolean isremoved = instituicaoService.removerInstituicao(codigo);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
