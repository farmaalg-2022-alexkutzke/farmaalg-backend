package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.dica.DicaAtualizacaoRequest;
import com.ufpr.farmaalg.request.dica.DicaCadastroRequest;
import com.ufpr.farmaalg.response.dica.DicaExercicioResponse;
import com.ufpr.farmaalg.service.DicaExercicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/exercicio/dica")
public class DicaExercicioController {

    @Autowired
    private DicaExercicioService dicaExercicioService;

    @GetMapping("exercicio/{codigoExercicio}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> ListarDicasExercicio(@PathVariable Long codigoExercicio, @RequestHeader("Authorization") String token) {
        List<DicaExercicioResponse> dicasExercicio = dicaExercicioService.buscarTodasDicasExercicio(codigoExercicio, token);

        if (dicasExercicio == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(dicasExercicio);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> buscarDicaExercicio(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        DicaExercicioResponse dicaExercicioResponse = dicaExercicioService.buscarDicaExercicioPorCodigo(codigo, token);

        if (dicaExercicioResponse == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(dicaExercicioResponse);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> cadastrarDicaExercicio(@RequestBody DicaCadastroRequest dicaExercicio, @RequestHeader("Authorization") String token) {
        DicaExercicioResponse dicaExercicioSalva = dicaExercicioService.cadastrarNovaDicaExercicio(dicaExercicio, token);

        if (dicaExercicioSalva == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(dicaExercicioSalva);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> atualizarDicaExercicio(@PathVariable Long codigo, @RequestBody DicaAtualizacaoRequest dicaExercicio, @RequestHeader("Authorization") String token) {
        DicaExercicioResponse dicaExercicioAtualizada = dicaExercicioService.atualizarDicaExercicio(codigo, dicaExercicio, token);

        if (dicaExercicioAtualizada == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(dicaExercicioAtualizada);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> removerDicaExercicio(@PathVariable Long codigo, @RequestHeader("Authorization") String token) {
        boolean isremoved = dicaExercicioService.removerDicaExercicio(codigo, token);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
