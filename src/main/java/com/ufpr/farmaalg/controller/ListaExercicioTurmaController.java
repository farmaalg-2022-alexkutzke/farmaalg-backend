package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.listaexercicio.ListaExercicioAtualizacaoRequest;
import com.ufpr.farmaalg.request.listaexercicio.ListaExercicioCadastroRequest;
import com.ufpr.farmaalg.response.listaexercicio.ListaExercicioTurmaResponse;
import com.ufpr.farmaalg.service.ListaExercicioTurmaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/lista/exercicio/")
public class ListaExercicioTurmaController {

    @Autowired
    private ListaExercicioTurmaService listaExercicioTurmaService;

    @GetMapping("/turma/{codigoTurma}")
    public ResponseEntity<?> ListarListaExerciciosTurma(@PathVariable Long codigoTurma) {
        List<ListaExercicioTurmaResponse> listasExercicios = listaExercicioTurmaService.buscarTodasListaExerciciosTurma(codigoTurma);

        if (listasExercicios == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(listasExercicios);
    }

    @GetMapping("/{codigoLista}")
    public ResponseEntity<?> buscarListaExercicioTurma(@PathVariable Long codigoLista) {
        ListaExercicioTurmaResponse listaExercicioTurma = listaExercicioTurmaService.buscarListaExercicioTurmaPorCodigo(codigoLista);

        return ResponseEntity.ok(listaExercicioTurma);
    }

    @PostMapping
    public ResponseEntity<?> cadastrarListaExercicioTurma(@RequestBody ListaExercicioCadastroRequest listaExercicioTurma) {
        try{
            ListaExercicioTurmaResponse listaExercicioTurmaSalva = listaExercicioTurmaService.cadastrarNovaListaExercicioTurma(listaExercicioTurma);

            return ResponseEntity.status(HttpStatus.CREATED).body(listaExercicioTurmaSalva);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }
    }

    @PutMapping("/{codigo}")
    public ResponseEntity<?> atualizarListaExercicioTurma(@PathVariable Long codigo, @RequestBody ListaExercicioAtualizacaoRequest listaExercicioTurma) {
        ListaExercicioTurmaResponse listaExercicioTurmaAtualizada = listaExercicioTurmaService.atualizarListaExercicioTurma(codigo, listaExercicioTurma);

        if (listaExercicioTurmaAtualizada == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(listaExercicioTurmaAtualizada);
    }

    @DeleteMapping("/{codigo}")
    public ResponseEntity<?> removerListaExercicioTurma(@PathVariable Long codigo) {
        boolean isremoved = listaExercicioTurmaService.removerListaExercicioTurma(codigo);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
