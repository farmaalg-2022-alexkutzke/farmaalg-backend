package com.ufpr.farmaalg.controller;

import com.ufpr.farmaalg.request.linguagem.LinguagemAtualizacaoRequest;
import com.ufpr.farmaalg.request.linguagem.LinguagemCadastroRequest;
import com.ufpr.farmaalg.response.linguagem.LinguagemResponse;
import com.ufpr.farmaalg.service.LinguagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Lia Alflen
 */

@RestController
@RequestMapping("/linguagem")
public class LinguagemController {

    @Autowired
    private LinguagemService linguagemService;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> ListarLinguagem() {
        List<LinguagemResponse> linguagem = linguagemService.buscarTodasLinguagens();

        return ResponseEntity.ok(linguagem);
    }

    @GetMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('PROFESSOR')")
    public ResponseEntity<?> buscarLinguagem(@PathVariable Long codigo) {
        LinguagemResponse linguagem = linguagemService.buscarLinguagemPorCodigo(codigo);

        return ResponseEntity.ok(linguagem);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> cadastrarLinguagem(@RequestBody LinguagemCadastroRequest linguagem) {
        LinguagemResponse linguagemSalva = linguagemService.cadastrarNovaLinguagem(linguagem);

        return ResponseEntity.status(HttpStatus.CREATED).body(linguagemSalva);
    }

    @PutMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> atualizarLinguagem(@PathVariable Long codigo, @RequestBody LinguagemAtualizacaoRequest linguagem) {
        LinguagemResponse linguagemAtualizada = linguagemService.atualizarLinguagem(codigo, linguagem);

        if (linguagemAtualizada == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok(linguagemAtualizada);
    }

    @DeleteMapping("/{codigo}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> removerLinguagem(@PathVariable Long codigo) {
        boolean isremoved = linguagemService.removerLinguagem(codigo);

        if (!isremoved) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Recurso com o id solicitado não encontrado.");
        }

        return ResponseEntity.ok("Recurso removido.");
    }
}
