package com.ufpr.farmaalg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Lia Alflen
 */

@SpringBootApplication
public class FarmaalgApplication {

	public static void main(String[] args) {
		SpringApplication.run(FarmaalgApplication.class, args);
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
}
