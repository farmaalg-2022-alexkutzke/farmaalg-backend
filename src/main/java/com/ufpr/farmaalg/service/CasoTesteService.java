package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.config.security.AuthenticationFilter;
import com.ufpr.farmaalg.model.*;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurma;
import com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste.RespostaCasoTeste;
import com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste.RespostaCasoTesteId;
import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.repository.CasoTesteRepository;
import com.ufpr.farmaalg.repository.RespostaCasoTesteRepository;
import com.ufpr.farmaalg.request.casoteste.CasoTesteAtualizacaoRequest;
import com.ufpr.farmaalg.request.casoteste.CasoTesteCadastroRequest;
import com.ufpr.farmaalg.request.casoteste.teste.TesteCasoTesteRequest;
import com.ufpr.farmaalg.request.sandbox.SandboxRequest;
import com.ufpr.farmaalg.response.casoteste.CasoTesteCompletoResponse;
import com.ufpr.farmaalg.response.casoteste.CasoTesteResponse;
import com.ufpr.farmaalg.response.exercicio.ExercicioResponse;
import com.ufpr.farmaalg.response.resposta.CasoTesteRespostaResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class CasoTesteService {

    @Autowired
    private CasoTesteRepository casoTesteRepository;

    @Autowired
    private ExercicioService exercicioService;

    @Autowired
    private RespostaService respostaService;

    @Autowired
    private RespostaCasoTesteRepository respostaCasoTesteRepository;

    @Autowired
    private LinguagemService linguagemService;

    @Autowired
    private SandboxService sandboxService;

    @Autowired
    private TurmaService turmaService;

    public List<CasoTesteCompletoResponse> buscarTodosCasosTesteDeUmExercicio(Long codigoExercicio, String token) {
        Optional<Exercicio> optionalExercicio = exercicioService.buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        //professor só pode visualizar casos de teste de seus exercícios
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalExercicio.get().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        //aluno só pode visualizar casos de teste de exercícios das turmas que está matriculado
        } else if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            List<AlunoTurma> alunoTurmas = turmaService.buscarTurmasDeUmAluno(idLogado);

            boolean estahMatriculado = false;
            for (AlunoTurma alunoTurma : alunoTurmas) {
                if (alunoTurma.getTurma().getId().equals(optionalExercicio.get().getTurma().getId())) {
                    estahMatriculado = true;
                }
            }

            if (!estahMatriculado) {
                return null;
            }
        }

        List<CasoTeste> casoTestes = buscarTodosCasosTestePorExercicio(optionalExercicio.get());

        List<CasoTesteCompletoResponse> response = new ArrayList<>();

        for (CasoTeste casoTeste : casoTestes) {
            ExercicioResponse exercicioResponse = exercicioService.converteExercicioParaResponse(casoTeste.getExercicio());

            response.add(
                    new CasoTesteCompletoResponse(casoTeste.getId(), casoTeste.getTitulo(),
                        casoTeste.getInput(), casoTeste.getOutput(), exercicioResponse)
            );
        }
        return response;
    }

    public CasoTesteResponse buscarCasoTestePorCodigo(Long codigo, String token) {
        Optional<CasoTeste> optionalCasoTeste = buscarCasoTesteDoBancoPorCodigo(codigo);

        if (optionalCasoTeste.isEmpty()) {
            return null;
        }

        //professor só pode visualizar casos de teste de seus exercícios
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalCasoTeste.get().getExercicio().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        return converteCasoTesteParaResponse(optionalCasoTeste.get());
    }

    public CasoTesteResponse cadastrarNovoCasoTeste(CasoTesteCadastroRequest casoTesteCadastroRequest, String token) {
        CasoTeste casoTeste = verificaRequestDeCasoTeste(casoTesteCadastroRequest.getExercicio().getId(), casoTesteCadastroRequest.getTitulo(), casoTesteCadastroRequest.getInput(), casoTesteCadastroRequest.getOutput(), null);
        if (casoTeste == null) return null;

        //professor só pode cadastrar casos de teste nos seus exercícios
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!casoTeste.getExercicio().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        return converteCasoTesteParaResponse(casoTesteRepository.save(casoTeste));
    }

    public CasoTesteResponse atualizarCasoTeste(Long codigo, CasoTesteAtualizacaoRequest casoTesteAtualizacaoRequest, String token) {
        Optional<CasoTeste> optionalCasoTeste = buscarCasoTesteDoBancoPorCodigo(codigo);

        if (optionalCasoTeste.isEmpty()) {
            return null;
        }

        CasoTeste casoTesteBanco = optionalCasoTeste.get();

        //professor só pode atualizar casos de teste dos seus exercícios
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!casoTesteBanco.getExercicio().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        CasoTeste casoTeste = verificaRequestDeCasoTeste(casoTesteAtualizacaoRequest.getExercicio().getId(), casoTesteAtualizacaoRequest.getTitulo(), casoTesteAtualizacaoRequest.getInput(), casoTesteAtualizacaoRequest.getOutput(), casoTesteAtualizacaoRequest.getId());
        if (casoTeste == null) return null;

        BeanUtils.copyProperties(casoTeste, casoTesteBanco, "id");

        return converteCasoTesteParaResponse(casoTesteRepository.save(casoTesteBanco));
    }

    @Transactional
    public boolean removerCasoTeste(Long codigo, String token) {
        Optional<CasoTeste> optionalCasoTeste = buscarCasoTesteDoBancoPorCodigo(codigo);

        if (optionalCasoTeste.isEmpty()) {
            return false;
        }

        if (token!=null) {
            //professor só pode remover casos de teste dos seus exercícios
            if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
                Long idLogado = AuthenticationFilter.getIdFromToken(token);

                if (!optionalCasoTeste.get().getExercicio().getTurma().getProfessor().getId().equals(idLogado)) {
                    return false;
                }
            }
        }

        List<RespostaCasoTeste> respostaCasosTeste = respostaCasoTesteRepository.findAllByCasoTeste(optionalCasoTeste.get());
        respostaCasosTeste.forEach(respostaCasoTeste -> removeRelacionamentoRespostaCasoTeste(respostaCasoTeste));

        casoTesteRepository.delete(optionalCasoTeste.get());
        return true;
    }

    protected RespostaCasoTeste adicionarCasoDeTesteAUmaResposta(CasoTeste casoTeste, Resposta resposta, boolean estahCorreto, String outputObtido) {
        RespostaCasoTesteId respostaCasoTesteId = new RespostaCasoTesteId(casoTeste.getId(), resposta.getId());
        RespostaCasoTeste respostaCasoTeste = new RespostaCasoTeste(respostaCasoTesteId, casoTeste, resposta, estahCorreto, outputObtido);

        return respostaCasoTesteRepository.save(respostaCasoTeste);
    }

    protected boolean removerCasoDeTesteDeUmaResposta(Long codigoCasoTeste, Long codigoResposta) {
        Optional<CasoTeste> optionalCasoTeste = buscarCasoTesteDoBancoPorCodigo(codigoCasoTeste);

        if (optionalCasoTeste.isEmpty()) {
            return false;
        }

        Optional<Resposta> optionalResposta = respostaService.buscarRespostaDoBancoPorCodigo(codigoResposta);

        if (optionalResposta.isEmpty()) {
            return false;
        }

        CasoTeste casoTeste = optionalCasoTeste.get();
        Resposta resposta = optionalResposta.get();

        RespostaCasoTesteId respostaCasoTesteId = new RespostaCasoTesteId(casoTeste.getId(), resposta.getId());
        RespostaCasoTeste respostaCasoTeste = respostaCasoTesteRepository.findById(respostaCasoTesteId);

        if (respostaCasoTeste == null) {
            return false;
        }

        respostaCasoTesteRepository.delete(respostaCasoTeste);

        return true;
    }

    private CasoTesteResponse converteCasoTesteParaResponse(CasoTeste casoTeste){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(casoTeste, CasoTesteResponse.class);
    }

    private Optional<CasoTeste> buscarCasoTesteDoBancoPorCodigo(Long codigo) {
        return casoTesteRepository.findById(codigo);
    }

    private CasoTeste verificaRequestDeCasoTeste(Long codigoExercicio, String titulo, String input, String output, Long codigoCasoTeste) {
        Optional<Exercicio> optionalExercicio = exercicioService.buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        return new CasoTeste(null, optionalExercicio.get(), titulo, input, output);
    }

    protected List<CasoTeste> buscarTodosCasosTestePorExercicio(Exercicio exercicio) {
        return casoTesteRepository.findAllByExercicio(exercicio);
    }

    protected void removeRelacionamentoRespostaCasoTeste(RespostaCasoTeste respostaCasoTeste) {
        respostaCasoTesteRepository.delete(respostaCasoTeste);
    }

    public CasoTesteRespostaResponse testarCasoTeste(TesteCasoTesteRequest testeCasoTesteRequest, Long codigo, String token) throws InterruptedException, IOException, URISyntaxException {
        Optional<CasoTeste> optionalCasoTeste = buscarCasoTesteDoBancoPorCodigo(codigo);

        if (optionalCasoTeste.isEmpty()) {
            return null;
        }

        //professor só testar casos de teste dos seus exercícios
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalCasoTeste.get().getExercicio().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        Optional<Linguagem> optionalLinguagem = linguagemService.buscarLinguagemDoBancoPorCodigo(testeCasoTesteRequest.getLinguagem().getId());

        if (optionalLinguagem.isEmpty()) {
            return null;
        }

        CasoTeste casoTeste = optionalCasoTeste.get();
        Linguagem linguagem = optionalLinguagem.get();

        SandboxRequest sandboxRequest = new SandboxRequest(linguagem.getCodigoSandbox(),
                testeCasoTesteRequest.getCodigo(), casoTeste.getInput() != null ? new String[]{casoTeste.getInput()} : null);

        String outputObtido = sandboxService.fazRequisicaoAoSandbox(sandboxRequest);

        return new CasoTesteRespostaResponse(
                casoTeste.getId(), casoTeste.getTitulo(),
                casoTeste.getInput(), casoTeste.getOutput(),
                outputObtido.equals(casoTeste.getOutput()), outputObtido
        );
    }
}
