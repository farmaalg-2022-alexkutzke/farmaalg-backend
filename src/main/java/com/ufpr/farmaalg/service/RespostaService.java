package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.config.security.AuthenticationFilter;
import com.ufpr.farmaalg.model.*;
import com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste.RespostaCasoTeste;
import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.model.util.Status;
import com.ufpr.farmaalg.repository.RespostaCasoTesteRepository;
import com.ufpr.farmaalg.repository.RespostaRepository;
import com.ufpr.farmaalg.request.resposta.RespostaAtualizacaoRequest;
import com.ufpr.farmaalg.request.resposta.RespostaCadastroRequest;
import com.ufpr.farmaalg.request.sandbox.SandboxRequest;
import com.ufpr.farmaalg.response.resposta.CasoTesteRespostaResponse;
import com.ufpr.farmaalg.response.resposta.RespostaResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class RespostaService {

    @Autowired
    private RespostaRepository respostaRepository;

    @Autowired
    private ExercicioService exercicioService;

    @Autowired
    private LinguagemService linguagemService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private TurmaService turmaService;

    @Autowired
    private RespostaCasoTesteRepository respostaCasoTesteRepository;

    @Autowired
    private CasoTesteService casoTesteService;

    @Autowired
    private SandboxService sandboxService;

    public List<RespostaResponse> buscarTodasRespostasDeUmExercicio(Long codigoExercicio, String token) {
        Exercicio exercicio = verificaPerfilProfessorEExercicio(codigoExercicio, token);
        if (exercicio == null) return null;

        List<Resposta> respostas = buscarRespostasDeUmExercicio(exercicio);

        List<RespostaResponse> response = new ArrayList<>();

        for (Resposta resposta : respostas) {
            response.add(converteRespostaParaResponse(resposta));
        }
        return response;
    }

    public List<RespostaResponse> buscarTodasRespostasDeUmAluno(Long codigoAluno, String token) {
        Usuario usuario = verificaPerfilAlunoEUsuario(codigoAluno, token);
        if (usuario == null) return null;

        List<Resposta> respostas;

        //Professor só pode ver respostas de exercícios dele
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            respostas = respostaRepository.findAllByAlunoAndExercicio_Turma_Professor_Id(usuario, idLogado);
        } else {
            respostas = buscarRespostasDeUmAluno(usuario);
        }

        List<RespostaResponse> response = new ArrayList<>();

        for (Resposta resposta : respostas) {
            response.add(converteRespostaParaResponse(resposta));
        }
        return response;
    }

    public List<RespostaResponse> buscarRespostasDeUmAlunoEmUmExercicio(Long codigoAluno, Long codigoExercicio, String token) {
        Usuario usuario = verificaPerfilAlunoEUsuario(codigoAluno, token);
        if (usuario == null) return null;

        Exercicio exercicio = verificaPerfilProfessorEExercicio(codigoExercicio, token);
        if (exercicio == null) return null;

        List<Resposta> respostas = respostaRepository.findAllByExercicioAndAluno(exercicio, usuario);

        List<RespostaResponse> response = new ArrayList<>();

        for (Resposta resposta : respostas) {
            response.add(converteRespostaParaResponse(resposta));
        }
        return response;
    }

    private Exercicio verificaPerfilProfessorEExercicio(Long codigoExercicio, String token) {
        Optional<Exercicio> optionalExercicio = exercicioService.buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        //Professor só pode ver respostas de exercícios dele
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalExercicio.get().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }
        return optionalExercicio.get();
    }

    private Usuario verificaPerfilAlunoEUsuario(Long codigoAluno, String token) {
        //Aluno só pode ver as suas respostas
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!codigoAluno.equals(idLogado)) {
                return null;
            }
        }

        Optional<Usuario> optionalUsuario = usuarioService.buscarUsuarioDoBancoPorCodigo(codigoAluno);

        if (optionalUsuario.isEmpty() || !optionalUsuario.get().getPerfil().equals(Perfil.ALUNO)) {
            return null;
        }
        return optionalUsuario.get();
    }

    public RespostaResponse buscarRespostaPorCodigo(Long codigo, String token) {
        Optional<Resposta> optionalResposta = buscarRespostaDoBancoPorCodigo(codigo);

        if (optionalResposta.isEmpty()) {
            return null;
        }

        //Aluno só pode ver as suas respostas
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalResposta.get().getAluno().getId().equals(idLogado)) {
                return null;
            }
            //Professor só pode ver respostas de exercícios dele
        } else if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalResposta.get().getExercicio().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        return converteRespostaParaResponse(optionalResposta.get());
    }

    @Transactional
    public RespostaResponse cadastrarNovaResposta(RespostaCadastroRequest respostaCadastroRequest, String token) throws URISyntaxException, IOException, InterruptedException {
        Resposta resposta = verificaRequestDeResposta(respostaCadastroRequest.getExercicio().getId(),
                respostaCadastroRequest.getLinguagem().getId(), respostaCadastroRequest.getAluno().getId(),
                null, respostaCadastroRequest.getCodigo(), 0, respostaCadastroRequest.isEhFinal(), token);
        if (resposta == null) return null;

        List<Resposta> respostas = respostaRepository.findAllByExercicioAndAluno(resposta.getExercicio(), resposta.getAluno());
        int tentativas = respostas.size();
        resposta.setNumeroTentativa(++tentativas);

        //Não permite mais submissão de respostas após flag ehFinal ter sido sinalizada
        for (Resposta r : respostas) {
            if (r.isEhFinal()) {
                return null;
            }
        }
        resposta = respostaRepository.save(resposta);

        List<CasoTesteRespostaResponse> casoTesteResponse = executaCodigoSandboxEValidaCasosTeste(resposta);

        RespostaResponse response = converteRespostaParaResponse(resposta);
        response.setCasosTeste(casoTesteResponse);
        return response;
    }

    private List<CasoTesteRespostaResponse> executaCodigoSandboxEValidaCasosTeste(Resposta resposta) throws URISyntaxException, IOException, InterruptedException {
        List<CasoTeste> casoTestes = casoTesteService.buscarTodosCasosTestePorExercicio(resposta.getExercicio());

        List<CasoTesteRespostaResponse> casoTesteResponse = new ArrayList<>();

        for (CasoTeste casoTeste : casoTestes) {
            SandboxRequest sandboxRequest = new SandboxRequest(resposta.getLinguagem().getCodigoSandbox(),
                    resposta.getCodigo(), casoTeste.getInput() != null ? new String[]{casoTeste.getInput()} : null);

            String outputObtido = sandboxService.fazRequisicaoAoSandbox(sandboxRequest);

            RespostaCasoTeste respostaCasoTeste = casoTesteService.adicionarCasoDeTesteAUmaResposta(casoTeste, resposta,
                    outputObtido.equals(casoTeste.getOutput()), outputObtido);

            casoTesteResponse.add(
                    new CasoTesteRespostaResponse(
                            respostaCasoTeste.getCasoTeste().getId(), respostaCasoTeste.getCasoTeste().getTitulo(),
                            respostaCasoTeste.getCasoTeste().getInput(), respostaCasoTeste.getCasoTeste().getOutput(),
                            respostaCasoTeste.isEstahCorreto(), respostaCasoTeste.getOutputObtido()
                    )
            );
        }
        return casoTesteResponse;
    }

    public RespostaResponse atualizarResposta(Long codigo, RespostaAtualizacaoRequest respostaAtualizacaoRequest, String token) {
        Optional<Resposta> optionalResposta = buscarRespostaDoBancoPorCodigo(codigo);

        if (optionalResposta.isEmpty()) {
            return null;
        }

        Resposta respostaBanco = optionalResposta.get();

        Resposta resposta = verificaRequestDeResposta(respostaAtualizacaoRequest.getExercicio().getId(),
                respostaAtualizacaoRequest.getLinguagem().getId(), respostaAtualizacaoRequest.getAluno().getId(),
                respostaAtualizacaoRequest.getId(), respostaAtualizacaoRequest.getCodigo(), respostaAtualizacaoRequest.getNumeroTentativa(),
                respostaAtualizacaoRequest.isEhFinal(), token);
        if (resposta == null) return null;

        //Aluno só pode modificar as suas respostas
        Long idLogado = AuthenticationFilter.getIdFromToken(token);

        if (!respostaBanco.getAluno().getId().equals(idLogado)) {
            return null;
        }

        BeanUtils.copyProperties(resposta, respostaBanco, "id", "casosTeste");

        return converteRespostaParaResponse(respostaRepository.save(respostaBanco));
    }

    @Transactional
    public boolean removerResposta(Long codigo, String token) {
        Optional<Resposta> optionalResposta = buscarRespostaDoBancoPorCodigo(codigo);

        if (optionalResposta.isEmpty()) {
            return false;
        }

        if (token != null) {
            //Aluno só pode remover as suas respostas
            if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
                Long idLogado = AuthenticationFilter.getIdFromToken(token);

                if (!optionalResposta.get().getAluno().getId().equals(idLogado)) {
                    return false;
                }
            }
        }

        List<RespostaCasoTeste> respostaCasosTeste = respostaCasoTesteRepository.findAllByResposta(optionalResposta.get());
        respostaCasosTeste.forEach(respostaCasoTeste -> casoTesteService.removeRelacionamentoRespostaCasoTeste(respostaCasoTeste));

        respostaRepository.delete(optionalResposta.get());
        return true;
    }

    private RespostaResponse converteRespostaParaResponse(Resposta resposta) {
        ModelMapper modelMapper = new ModelMapper();

        List<CasoTesteRespostaResponse> casoTesteLista = new ArrayList<>();

        if (resposta.getCasosTeste() != null) {
            resposta.getCasosTeste().forEach(casoTeste -> {
                casoTesteLista.add(new CasoTesteRespostaResponse(casoTeste.getCasoTeste().getId(),
                        casoTeste.getCasoTeste().getTitulo(), casoTeste.getCasoTeste().getInput(),
                        casoTeste.getCasoTeste().getOutput(), casoTeste.isEstahCorreto(), casoTeste.getOutputObtido()));
            });
        }

        RespostaResponse response = modelMapper.map(resposta, RespostaResponse.class);
        response.setCasosTeste(casoTesteLista);

        return response;
    }

    private Resposta verificaRequestDeResposta(Long codigoExercicio, Long codigoLinguagem, Long codigoAluno, Long codigoResposta, String codigo, int numeroTentativa, boolean ehFinal, String token) {
        Optional<Exercicio> optionalExercicio = exercicioService.buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        Optional<Linguagem> optionalLinguagem = linguagemService.buscarLinguagemDoBancoPorCodigo(codigoLinguagem);

        if (optionalLinguagem.isEmpty()) {
            return null;
        }

        if (!exercicioService.verificaSeExercicioPermiteLinguagem(optionalExercicio.get(), optionalLinguagem.get())) {
            return null;
        }

        Turma turma = optionalExercicio.get().getTurma();

        Optional<Usuario> optionalUsuario = usuarioService.buscarUsuarioDoBancoPorCodigo(codigoAluno);

        if (optionalUsuario.isEmpty() || !AuthenticationFilter.getIdFromToken(token).equals(optionalUsuario.get().getId())) {
            return null;
        }

        if (!turmaService.verificarSeAlunoMatriculadoNaTurma(optionalUsuario.get(), turma)) {
            return null;
        }

        if (!turma.getStatus().equals(Status.ATIVA)) {
            return null;
        }

        return new Resposta(codigoResposta, codigo, numeroTentativa, ehFinal, optionalUsuario.get(), optionalExercicio.get(), optionalLinguagem.get(), null);
    }

    protected Optional<Resposta> buscarRespostaDoBancoPorCodigo(Long codigo) {
        return respostaRepository.findById(codigo);
    }

    protected List<Resposta> buscarRespostasDeUmAluno(Usuario aluno) {
        return respostaRepository.findAllByAluno(aluno);
    }

    protected List<Resposta> buscarRespostasDeUmExercicio(Exercicio exercicio) {
        return respostaRepository.findAllByExercicio(exercicio);
    }

    protected List<Resposta> buscarRespostasDeUmaLinguagem(Linguagem linguagem) {
        return respostaRepository.findAllByLinguagem(linguagem);
    }
}
