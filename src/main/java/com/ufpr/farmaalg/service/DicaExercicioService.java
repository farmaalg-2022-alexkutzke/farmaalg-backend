package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.config.security.AuthenticationFilter;
import com.ufpr.farmaalg.model.DicaExercicio;
import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.Usuario;
import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.repository.DicaExercicioRepository;
import com.ufpr.farmaalg.request.dica.DicaAtualizacaoRequest;
import com.ufpr.farmaalg.request.dica.DicaCadastroRequest;
import com.ufpr.farmaalg.response.dica.DicaExercicioResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class DicaExercicioService {

    @Autowired
    private DicaExercicioRepository dicaExercicioRepository;

    @Autowired
    private ExercicioService exercicioService;

    @Autowired
    private UsuarioService usuarioService;

    public List<DicaExercicioResponse> buscarTodasDicasExercicio(Long codigoExercicio, String token) {
        Optional<Exercicio> optionalExercicio = exercicioService.buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        //Professor só pode buscar dicas de exercícios de sua instituição
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            Optional<Usuario> optionalUsuarioLogado = usuarioService.buscarUsuarioDoBancoPorCodigo(idLogado);

            if (optionalUsuarioLogado.isEmpty()) {
                return null;
            }

            if (!optionalExercicio.get().getTurma().getProfessor().getInstituicao().equals(optionalUsuarioLogado.get().getInstituicao())) {
                return null;
            }
        }

        List<DicaExercicio> dicasExercicio = findDicaExercicioByExercicio(optionalExercicio.get());

        List<DicaExercicioResponse> response = new ArrayList<>();

        for (DicaExercicio dicaExercicio : dicasExercicio) {
            response.add(converteDicaExercicioParaResponse(dicaExercicio));
        }
        return response;
    }

    public DicaExercicioResponse buscarDicaExercicioPorCodigo(Long codigo, String token) {
        Optional<DicaExercicio> optionalDicaExercicio = buscarDicaExercicioDoBancoPorCodigo(codigo);

        if (optionalDicaExercicio.isPresent()) {
            //Professor só pode buscar dicas de exercícios de sua instituição
            if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
                Long idLogado = AuthenticationFilter.getIdFromToken(token);

                Optional<Usuario> optionalUsuarioLogado = usuarioService.buscarUsuarioDoBancoPorCodigo(idLogado);

                if (optionalUsuarioLogado.isEmpty()) {
                    return null;
                }

                if (!optionalDicaExercicio.get().getExercicio().getTurma().getProfessor().getInstituicao().equals(optionalUsuarioLogado.get().getInstituicao())) {
                    return null;
                }
            }

            return converteDicaExercicioParaResponse(optionalDicaExercicio.get());
        }else{
            return null;
        }
    }

    public DicaExercicioResponse cadastrarNovaDicaExercicio(DicaCadastroRequest dicaCadastroRequest, String token) {
        Exercicio exercicio = verificaExercicioEPerfilNaDica(dicaCadastroRequest.getExercicio().getId(), dicaCadastroRequest.getNumeroTentativas(), token);
        if (exercicio == null) return null;

        return converteDicaExercicioParaResponse(criaECadastraDicaExercicio(exercicio, dicaCadastroRequest.getTitulo(), dicaCadastroRequest.getDescricao(), dicaCadastroRequest.getNumeroTentativas()));
    }

    public DicaExercicioResponse atualizarDicaExercicio(Long codigo, DicaAtualizacaoRequest dicaAtualizacaoRequest, String token) {
        Exercicio exercicio = verificaExercicioEPerfilNaDica(dicaAtualizacaoRequest.getExercicio().getId(), dicaAtualizacaoRequest.getNumeroTentativas(), token);
        if (exercicio == null) return null;

        Optional<DicaExercicio> optionalDicaExercicio = buscarDicaExercicioDoBancoPorCodigo(codigo);

        if (optionalDicaExercicio.isEmpty()) {
            return null;
        }

        DicaExercicio dicaExercicioBanco = optionalDicaExercicio.get();

        DicaExercicio dicaExercicio = new DicaExercicio(dicaAtualizacaoRequest.getId(), exercicio, dicaAtualizacaoRequest.getTitulo(), dicaAtualizacaoRequest.getDescricao(), dicaAtualizacaoRequest.getNumeroTentativas());

        BeanUtils.copyProperties(dicaExercicio, dicaExercicioBanco, "id");

        return converteDicaExercicioParaResponse(cadastraDicaExercicioNoBanco(dicaExercicioBanco));
    }

    private Exercicio verificaExercicioEPerfilNaDica(Long codigoExercicio, int numeroTentativas, String token) {
        Optional<Exercicio> optionalExercicio = exercicioService.buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        //Professor só pode cadastrar/atualizar dicas em exercícios que pertençam a ele
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalExercicio.get().getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        if (numeroTentativas < 0) {
            return null;
        }
        return optionalExercicio.get();
    }

    public boolean removerDicaExercicio(Long codigo, String token) {
        DicaExercicio dica = null;

        //Professor só pode remover dicas de exercícios que pertençam a ele
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            dica = dicaExercicioRepository.findByIdAndExercicio_Turma_Professor_Id(codigo, idLogado);

            if (dica == null) {
                return false;
            }
        //Admin pode remover dica de qualquer exercício
        }else {
            Optional<DicaExercicio> optionalDicaExercicio = buscarDicaExercicioDoBancoPorCodigo(codigo);

            if (optionalDicaExercicio.isEmpty()) {
                return false;
            }

            dica = optionalDicaExercicio.get();
        }

        dicaExercicioRepository.delete(dica);
        return true;
    }

    private DicaExercicioResponse converteDicaExercicioParaResponse(DicaExercicio dicaExercicio){
        ModelMapper modelMapper = new ModelMapper();

        return modelMapper.map(dicaExercicio, DicaExercicioResponse.class);
    }

    private Optional<DicaExercicio> buscarDicaExercicioDoBancoPorCodigo(Long codigo) {
        return dicaExercicioRepository.findById(codigo);
    }

    protected List<DicaExercicio> findDicaExercicioByExercicio(Exercicio exercicio) {
        return dicaExercicioRepository.findAllByExercicio(exercicio);
    }

    protected void RemoverListaDeDicaExercicio(List<DicaExercicio> dicasExercicio) {
        dicaExercicioRepository.deleteAll(dicasExercicio);
    }

    protected DicaExercicio cadastraDicaExercicioNoBanco(DicaExercicio dicaExercicio) {
        return dicaExercicioRepository.save(dicaExercicio);
    }

    protected DicaExercicio criaECadastraDicaExercicio(Exercicio exercicio, String titulo, String descricao, int numeroTentativas) {
        DicaExercicio dicaExercicio = new DicaExercicio(null, exercicio, titulo, descricao, numeroTentativas);

        return cadastraDicaExercicioNoBanco(dicaExercicio);
    }
}
