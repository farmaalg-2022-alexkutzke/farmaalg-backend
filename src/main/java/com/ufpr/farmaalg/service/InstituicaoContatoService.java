package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.model.Instituicao;
import com.ufpr.farmaalg.model.InstituicaoContato;
import com.ufpr.farmaalg.repository.InstituicaoContatoRepository;
import com.ufpr.farmaalg.request.instituicaocontato.InstituicaoContatoAtualizacaoRequest;
import com.ufpr.farmaalg.request.instituicaocontato.InstituicaoContatoCadastroRequest;
import com.ufpr.farmaalg.response.instituicaocontato.InstituicaoContatoResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class InstituicaoContatoService {

    @Autowired
    private InstituicaoContatoRepository instituicaoContatoRepository;

    @Autowired
    private InstituicaoService instituicaoService;

    public List<InstituicaoContatoResponse> buscarTodasInstituicoesContatos() {
        List<InstituicaoContato> instituicaoContatos = instituicaoContatoRepository.findAll();

        List<InstituicaoContatoResponse> response = new ArrayList<>();

        for (InstituicaoContato instituicaoContato : instituicaoContatos) {
            response.add(converteInstituicaoContatoParaResponse(instituicaoContato));
        }
        return response;
    }

    public InstituicaoContatoResponse buscarInstituicaoContatoPorCodigo(Long codigo) {
        Optional<InstituicaoContato> optionalInstituicaoContato = buscarInstituicaoContatoDoBancoPorCodigo(codigo);

        if (optionalInstituicaoContato.isPresent()) {
            return converteInstituicaoContatoParaResponse(optionalInstituicaoContato.get());
        }else{
            return null;
        }
    }

    public InstituicaoContatoResponse cadastrarNovaInstituicaoContato(InstituicaoContatoCadastroRequest instituicaoContatoCadastroRequest) {
        Optional<Instituicao> optionalInstituicao = instituicaoService.buscarInstituicaoDoBancoPorCodigo(instituicaoContatoCadastroRequest.getInstituicao().getId());

        if (optionalInstituicao.isEmpty()) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();
        InstituicaoContato instituicaoContato = modelMapper.map(instituicaoContatoCadastroRequest, InstituicaoContato.class);

        instituicaoContato.setId(null);
        instituicaoContato.setInstituicao(optionalInstituicao.get());

        return converteInstituicaoContatoParaResponse(salvarInstituicaoContato(instituicaoContato));
    }

    public InstituicaoContatoResponse atualizarInstituicaoContato(Long codigo, InstituicaoContatoAtualizacaoRequest instituicaoContatoAtualizacaoRequest) {
        Optional<InstituicaoContato> optionalInstituicaoContato = buscarInstituicaoContatoDoBancoPorCodigo(codigo);

        if (optionalInstituicaoContato.isEmpty()) {
            return null;
        }

        InstituicaoContato instituicaoContatoBanco = optionalInstituicaoContato.get();

        ModelMapper modelMapper = new ModelMapper();
        InstituicaoContato instituicaoContato = modelMapper.map(instituicaoContatoAtualizacaoRequest, InstituicaoContato.class);
        instituicaoContato.setInstituicao(instituicaoContatoBanco.getInstituicao());

        BeanUtils.copyProperties(instituicaoContato, instituicaoContatoBanco, "id");

        return converteInstituicaoContatoParaResponse(instituicaoContatoRepository.save(instituicaoContatoBanco));
    }

    public boolean removerInstituicaoContato(Long codigo) {
        Optional<InstituicaoContato> optionalInstituicaoContato = buscarInstituicaoContatoDoBancoPorCodigo(codigo);

        if (optionalInstituicaoContato.isEmpty()) {
            return false;
        }

        instituicaoContatoRepository.delete(optionalInstituicaoContato.get());
        return true;
    }

    protected InstituicaoContatoResponse converteInstituicaoContatoParaResponse(InstituicaoContato instituicaoContato){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(instituicaoContato, InstituicaoContatoResponse.class);
    }

    protected InstituicaoContato salvarInstituicaoContato(InstituicaoContato instituicaoContato) {
        return instituicaoContatoRepository.save(instituicaoContato);
    }

    protected List<InstituicaoContato> buscarTodosContatosDeUmaInstituicao(Instituicao instituicao) {
        return instituicaoContatoRepository.findAllByInstituicao(instituicao);
    }

    private Optional<InstituicaoContato> buscarInstituicaoContatoDoBancoPorCodigo(Long codigo) {
        return instituicaoContatoRepository.findById(codigo);
    }
}
