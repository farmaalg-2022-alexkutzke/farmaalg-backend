package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.model.Instituicao;
import com.ufpr.farmaalg.model.InstituicaoContato;
import com.ufpr.farmaalg.repository.InstituicaoRepository;
import com.ufpr.farmaalg.request.instituicao.InstituicaoAtualizacaoRequest;
import com.ufpr.farmaalg.request.instituicao.InstituicaoCadastroRequest;
import com.ufpr.farmaalg.request.instituicao.InstituicaoCompletaCadastroRequest;
import com.ufpr.farmaalg.request.instituicao.InstituicaoCompletaContatoCadastroRequest;
import com.ufpr.farmaalg.response.instituicao.InstituicaoComboResponse;
import com.ufpr.farmaalg.response.instituicao.InstituicaoResponse;
import com.ufpr.farmaalg.response.instituicao.InstituicaoResponseContato;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class InstituicaoService {

    @Autowired
    private InstituicaoRepository instituicaoRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private InstituicaoContatoService instituicaoContatoService;

    public List<InstituicaoResponse> buscarTodasInstituicoes() {
        List<Instituicao> instituicoes = instituicaoRepository.findAll();

        List<InstituicaoResponse> response = new ArrayList<>();

        for (Instituicao instituicao : instituicoes) {
            response.add(converteInstituicaoParaResponse(instituicao));
        }
        return response;
    }

    public List<InstituicaoComboResponse> buscarComboInstituicoes() {
        List<Instituicao> instituicoes = instituicaoRepository.findAll();

        List<InstituicaoComboResponse> response = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();

        for (Instituicao instituicao : instituicoes) {
            response.add(modelMapper.map(instituicao, InstituicaoComboResponse.class));
        }

        return response;
    }

    public InstituicaoResponse buscarInstituicaoPorCodigo(Long codigo) {
        Optional<Instituicao> optionalInstituicao = buscarInstituicaoDoBancoPorCodigo(codigo);

        if (optionalInstituicao.isPresent()) {
            return converteInstituicaoParaResponse(optionalInstituicao.get());
        }else{
            return null;
        }
    }

    public InstituicaoResponse cadastrarNovaInstituicao(InstituicaoCadastroRequest instituicaoCadastroRequest) {
        ModelMapper modelMapper = new ModelMapper();

        Instituicao instituicao = modelMapper.map(instituicaoCadastroRequest, Instituicao.class);

        instituicao.setId(null);
        instituicao.setContatos(new ArrayList<>());
        return converteInstituicaoParaResponse(instituicaoRepository.save(instituicao));
    }

    @Transactional
    public InstituicaoResponse cadastrarNovaInstituicaoCompleta(InstituicaoCompletaCadastroRequest instituicaoCompleta) {
        ModelMapper modelMapper = new ModelMapper();

        Instituicao instituicao = modelMapper.map(instituicaoCompleta, Instituicao.class);
        instituicao.setId(null);
        instituicao.setContatos(null);
        instituicao = instituicaoRepository.save(instituicao);

        List<InstituicaoResponseContato> instituicaoResponseContato = new ArrayList<>();

        if (instituicaoCompleta.getContatos() != null) {
            for (InstituicaoCompletaContatoCadastroRequest contato : instituicaoCompleta.getContatos()) {
                InstituicaoContato instituicaoContato = modelMapper.map(contato, InstituicaoContato.class);
                instituicaoContato.setInstituicao(instituicao);
                instituicaoContato.setId(null);
                instituicaoContato = instituicaoContatoService.salvarInstituicaoContato(instituicaoContato);

                instituicaoResponseContato.add(converteInstituicaoContatoParaResponse(instituicaoContato));
            }
        }

        InstituicaoResponse instituicaoResponse = converteInstituicaoParaResponse(instituicao);
        instituicaoResponse.setContatos(instituicaoResponseContato);

        return instituicaoResponse;
    }

    public InstituicaoResponse atualizarInstituicao(Long codigo, InstituicaoAtualizacaoRequest instituicaoAtualizacaoRequest) {
        Optional<Instituicao> optionalInstituicao = buscarInstituicaoDoBancoPorCodigo(codigo);

        if (optionalInstituicao.isEmpty()) {
            return null;
        }
        Instituicao instituicaoBanco = optionalInstituicao.get();

        ModelMapper modelMapper = new ModelMapper();

        Instituicao instituicao = modelMapper.map(instituicaoAtualizacaoRequest, Instituicao.class);

        BeanUtils.copyProperties(instituicao, instituicaoBanco, "id");

        return converteInstituicaoParaResponse(instituicaoRepository.save(instituicaoBanco));
    }

    public boolean removerInstituicao(Long codigo) {
        Optional<Instituicao> optionalInstituicao = buscarInstituicaoDoBancoPorCodigo(codigo);

        if (optionalInstituicao.isEmpty()) {
            return false;
        }

        optionalInstituicao.get().getUsuarios().forEach(usuario -> usuarioService.removeUsuario(usuario.getId()));

        List<InstituicaoContato> contatos = instituicaoContatoService.buscarTodosContatosDeUmaInstituicao(optionalInstituicao.get());
        contatos.forEach(contato -> instituicaoContatoService.removerInstituicaoContato(contato.getId()));

        instituicaoRepository.delete(optionalInstituicao.get());
        return true;
    }

    private InstituicaoResponse converteInstituicaoParaResponse(Instituicao instituicao){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(instituicao, InstituicaoResponse.class);
    }

    protected Optional<Instituicao> buscarInstituicaoDoBancoPorCodigo(Long codigo) {
        return instituicaoRepository.findById(codigo);
    }

    private InstituicaoResponseContato converteInstituicaoContatoParaResponse(InstituicaoContato instituicaoContato) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(instituicaoContato, InstituicaoResponseContato.class);
    }
}
