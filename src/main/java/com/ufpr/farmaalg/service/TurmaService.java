package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.config.security.AuthenticationFilter;
import com.ufpr.farmaalg.model.*;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurma;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurmaId;
import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.model.util.Status;
import com.ufpr.farmaalg.repository.AlunoTurmaRepository;
import com.ufpr.farmaalg.repository.TurmaRepository;
import com.ufpr.farmaalg.request.turma.TurmaAtualizacaoRequest;
import com.ufpr.farmaalg.request.turma.CodigoAcessoRequest;
import com.ufpr.farmaalg.request.turma.TurmaCadastroRequest;
import com.ufpr.farmaalg.response.exercicio.ExercicioResponse;
import com.ufpr.farmaalg.response.exercicio.ExercicioSimplificadoResponse;
import com.ufpr.farmaalg.response.turma.TurmaCompletaResponse;
import com.ufpr.farmaalg.response.turma.TurmaResponse;
import com.ufpr.farmaalg.response.turma.UsuarioResponseSemDetalhes;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Lia Alflen
 */

@Service
public class TurmaService {

    @Autowired
    private TurmaRepository turmaRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private AlunoTurmaRepository alunoTurmaRepository;

    @Autowired
    private ExercicioService exercicioService;

    @Autowired
    private ListaExercicioTurmaService listaExercicioTurmaService;

    public List<TurmaResponse> buscarTodasTurmas(String token) {
        List<Turma> turmas;

        //se for professor, mostra apenas as turmas que ele possui
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            turmas = turmaRepository.findAllByProfessor_Id(idLogado);

        //se for aluno, mostra apenas as turmas inscritas
        } else if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            List<AlunoTurma> alunoTurmas = alunoTurmaRepository.findByAluno_Id(idLogado);

            List<Long> idsTurmas = alunoTurmas.stream().map(AlunoTurma::getId).map(AlunoTurmaId::getIdTurma).collect(Collectors.toList());

            turmas = turmaRepository.findAllByIdIn(idsTurmas);

        //se for admin, mostra todas as turmas de todas as instituicoes
        }else {
            turmas = turmaRepository.findAll();
        }

        List<TurmaResponse> response = new ArrayList<>();

        for (Turma turma : turmas) {
            response.add(converteTurmaParaResponse(turma));
        }

        return response;
    }

    public TurmaCompletaResponse buscarTurmaPorCodigo(Long codigo, String token) {
        Optional<Turma> optionalTurma = buscarTurmaDoBancoPorCodigo(codigo);

        if (optionalTurma.isEmpty()) {
            return null;
        }

        //professor pode buscar apenas suas turmas
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalTurma.get().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        //aluno pode buscar apenas turmas em que está matriculado
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            boolean alunoEstahMatriculado = false;

            for (AlunoTurma alunoTurma : optionalTurma.get().getAlunoTurmas()) {
                if (alunoTurma.getAluno().getId().equals(idLogado)) {
                    alunoEstahMatriculado = true;
                    break;
                }
            }

            if (!alunoEstahMatriculado) {
                return null;
            }

            return converteTurmaParaResponseCompleta(optionalTurma.get(), false);
        }

        return converteTurmaParaResponseCompleta(optionalTurma.get(), true);
    }

    public TurmaResponse cadastrarNovaTurma(TurmaCadastroRequest turmaCadastroRequest, String token) {
        Optional<Usuario> optionalUsuario = usuarioService.buscarUsuarioDoBancoPorCodigo(turmaCadastroRequest.getProfessor().getId());

        if (optionalUsuario.isEmpty() ) {
            return null;
        }

        //professor só pode criar turmas para si mesmo
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!optionalUsuario.get().getId().equals(idLogado)) {
                return null;
            }
        }

        if (!consultarDisponibilidadeCodigoAcesso(turmaCadastroRequest.getCodigoAcesso())) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();
        Turma turma = modelMapper.map(turmaCadastroRequest, Turma.class);

        turma.setId(null);
        turma.setProfessor(optionalUsuario.get());

        return converteTurmaParaResponse(turmaRepository.save(turma));
    }

    public TurmaResponse atualizarTurma(Long codigo, TurmaAtualizacaoRequest turmaAtualizacaoRequest, String token) {
        Turma turmaBanco = getTurmaDoBancoPorCodigo(codigo);
        if (turmaBanco == null) return null;

        //professor só pode editar suas próprias turmas
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!turmaBanco.getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        if (!turmaBanco.getCodigoAcesso().equals(turmaAtualizacaoRequest.getCodigoAcesso())) {
            if (!consultarDisponibilidadeCodigoAcesso(turmaAtualizacaoRequest.getCodigoAcesso())) {
                return null;
            }
        }

        Optional<Usuario> optionalUsuario = usuarioService.buscarUsuarioDoBancoPorCodigo(turmaAtualizacaoRequest.getProfessor().getId());

        if (optionalUsuario.isEmpty() ) {
            return null;
        }

        ModelMapper modelMapper = new ModelMapper();
        Turma turma = modelMapper.map(turmaAtualizacaoRequest, Turma.class);
        turma.setProfessor(optionalUsuario.get());

        //Todo: não permitir editar turmas encerradas.

        BeanUtils.copyProperties(turma, turmaBanco, "id");

        return converteTurmaParaResponse(turmaRepository.save(turmaBanco));
    }

    @Transactional
    public boolean removerTurma(Long codigo, String token) {
        Turma turmaBanco = getTurmaDoBancoPorCodigo(codigo);

        if (turmaBanco == null) {
            return false;
        }

        //Professor pode remover apenas turmas que ele gerencia.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!idLogado.equals(turmaBanco.getProfessor().getId())) {
                return false;
            }
        }

        removerTurmaERelacionamentos(turmaBanco);
        return true;
    }

    public TurmaResponse cadastrarAlunoTurma(CodigoAcessoRequest codigoAcessoRequest, Long codigoAluno, String token) {
        Optional<Usuario> optionalUsuario = usuarioService.buscarUsuarioDoBancoPorCodigo(codigoAluno);

        //usuario sendo cadastrado na turma tem que ter o perfil de Aluno
        if (optionalUsuario.isEmpty() || !optionalUsuario.get().getPerfil().equals(Perfil.ALUNO)) {
            return null;
        }

        Usuario usuarioBanco = optionalUsuario.get();

        //aluno só pode cadastrar a si mesmo na turma
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!usuarioBanco.getId().equals(idLogado)) {
                return null;
            }
        }

        //busca apenas turmas ativas e da mesma instituição para o aluno se cadastrar
        Turma turmaBanco = turmaRepository.findByCodigoAcessoAndStatusAndProfessor_Instituicao(codigoAcessoRequest.getCodigoAcesso(), Status.ATIVA, usuarioBanco.getInstituicao());

        if (turmaBanco == null) {
            return null;
        }

        AlunoTurmaId alunoTurmaId = new AlunoTurmaId(turmaBanco.getId(), usuarioBanco.getId());
        AlunoTurma alunoTurma = new AlunoTurma(alunoTurmaId, turmaBanco, usuarioBanco);

        alunoTurmaRepository.save(alunoTurma);

        turmaBanco.getAlunoTurmas().add(alunoTurma);

        return converteTurmaParaResponse(turmaBanco);
    }

    public boolean removerAlunoTurma(Long codigoTurma, Long codigoAluno, String token) {
        Optional<Usuario> optionalUsuario = usuarioService.buscarUsuarioDoBancoPorCodigo(codigoAluno);

        if (optionalUsuario.isEmpty()) {
            return false;
        }

        Usuario usuarioBanco = optionalUsuario.get();

        //aluno só pode remover a si mesmo na turma
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!usuarioBanco.getId().equals(idLogado)) {
                return false;
            }
        }

        Turma turmaBanco = getTurmaDoBancoPorCodigo(codigoTurma);

        if (turmaBanco == null) {
            return false;
        }

        //professor só pode remover aluno em suas turmas.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!turmaBanco.getProfessor().getId().equals(idLogado)) {
                return false;
            }
        }

        AlunoTurmaId alunoTurmaId = new AlunoTurmaId(turmaBanco.getId(), usuarioBanco.getId());
        AlunoTurma alunoTurma = new AlunoTurma(alunoTurmaId, turmaBanco, usuarioBanco);

        //Todo: verificar se aluno está presente na turma

        alunoTurmaRepository.delete(alunoTurma);

        turmaBanco.getAlunoTurmas().remove(alunoTurma);

        return true;
    }

    protected TurmaResponse converteTurmaParaResponse(Turma turma){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(turma, TurmaResponse.class);
    }

    private TurmaCompletaResponse converteTurmaParaResponseCompleta(Turma turma, boolean colocarAlunos) {
        ModelMapper modelMapper = new ModelMapper();

        List<ExercicioSimplificadoResponse> exercicios = new ArrayList<>();
        if (turma.getExercicios() != null) {
            turma.getExercicios().forEach(exercicio -> {
                ExercicioResponse exercicioResponse = exercicioService.converteExercicioParaResponse(exercicio);
                exercicios.add(modelMapper.map(exercicioResponse, ExercicioSimplificadoResponse.class));
            });
        }

        List<UsuarioResponseSemDetalhes> alunos = new ArrayList<>();

        if (colocarAlunos) {
            if (turma.getAlunoTurmas() != null) {
                turma.getAlunoTurmas().forEach(alunoTurma -> {
                    if (alunoTurma.getAluno() != null) {
                        alunos.add(new UsuarioResponseSemDetalhes(alunoTurma.getAluno().getId(), alunoTurma.getAluno().getPerfil(), alunoTurma.getAluno().getNome(), alunoTurma.getAluno().getEmail(), alunoTurma.getAluno().getAvatar()));
                    }
                });
            }
        }

        turma.setAlunoTurmas(null);
        turma.setExercicios(null);

        TurmaCompletaResponse response = modelMapper.map(turma, TurmaCompletaResponse.class);
        response.setExercicios(exercicios);
        response.setAlunos(alunos);

        return response;
    }

    private void removerTurmaERelacionamentos(Turma turmaBanco) {
        List<AlunoTurma> alunoTurmas = alunoTurmaRepository.findByTurma(turmaBanco);
        alunoTurmaRepository.deleteAll(alunoTurmas);

        turmaBanco.getListasExercicios().forEach(lista -> listaExercicioTurmaService.removerListaExercicioTurma(lista.getId()));

        setaNullExerciciosQueReferenciamExerciciosSendoExcluidos(turmaBanco);
        List<Exercicio> exerciciosTurma = exercicioService.listaTodosExerciciosPorTurma(turmaBanco);
        exerciciosTurma.forEach(exercicio -> exercicioService.removeExercicioERelacionamentos(exercicio.getId(), null));

        turmaRepository.delete(turmaBanco);
    }

    private Turma getTurmaDoBancoPorCodigo(Long codigo) {
        Optional<Turma> optionalTurma = buscarTurmaDoBancoPorCodigo(codigo);

        if (optionalTurma.isEmpty()) {
            return null;
        }

        return optionalTurma.get();
    }

    private void setaNullExerciciosQueReferenciamExerciciosSendoExcluidos(Turma turmaBanco) {
        List<Exercicio> exercicios = turmaBanco.getExercicios();

        List<Long> ids = new ArrayList<>();
        exercicios.forEach(exercicio -> ids.add(exercicio.getId()));

        List<Exercicio> exerciciosQueReferenciamExercicioSendoExcluido = exercicioService.buscarExercicioPorExercicioOriginalId(ids);
        exerciciosQueReferenciamExercicioSendoExcluido.forEach(exercicio -> exercicio.setExercicioOriginal(null));

        exercicioService.atualizarExercicios(exerciciosQueReferenciamExercicioSendoExcluido);
    }

    protected boolean verificarSeAlunoMatriculadoNaTurma(Usuario aluno, Turma turma) {
        AlunoTurma alunoTurma = alunoTurmaRepository.findByAlunoAndTurma(aluno, turma);

        if (alunoTurma==null) {
            return false;
        }
        return true;
    }

    protected boolean removeTurmaERelacionamentosBuscandoTurma(Long codigoTurma) {
        Turma turmaBanco = getTurmaDoBancoPorCodigo(codigoTurma);

        if (turmaBanco == null) {
            return false;
        }

        removerTurmaERelacionamentos(turmaBanco);
        return true;
    }

    protected Optional<Turma> buscarTurmaDoBancoPorCodigo(Long codigo) {
        return turmaRepository.findById(codigo);
    }

    public List<TurmaResponse> buscarTurmasDoAlunoPorToken(String token) {
        Long idLogado = AuthenticationFilter.getIdFromToken(token);

        List<AlunoTurma> alunoTurmas = alunoTurmaRepository.findByAluno_Id(idLogado);

        List<Long> idsTurmas = alunoTurmas.stream().map(AlunoTurma::getId).map(AlunoTurmaId::getIdTurma).collect(Collectors.toList());

        List<Turma> turmas = turmaRepository.findAllByIdIn(idsTurmas);

        List<TurmaResponse> turmasResponse = new ArrayList<>();
        turmas.forEach(turma -> turmasResponse.add(converteTurmaParaResponse(turma)));
        return turmasResponse;
    }

    protected List<AlunoTurma> buscarTurmasDeUmAluno(Long codigoAluno) {
        return alunoTurmaRepository.findByAluno_Id(codigoAluno);
    }

    public boolean consultarDisponibilidadeCodigoAcesso(String codigoAcesso) {
        if (codigoAcesso == null ||  codigoAcesso.isEmpty()) {
            return false;
        }

        Turma turma = turmaRepository.findByCodigoAcessoAndStatusIn(codigoAcesso, new ArrayList<>(Arrays.asList(Status.ATIVA, Status.PAUSADA)));

        if (turma == null) {
            return true;
        }
        return false;
    }
}
