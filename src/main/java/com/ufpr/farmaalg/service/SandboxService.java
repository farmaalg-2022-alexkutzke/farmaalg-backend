package com.ufpr.farmaalg.service;

import com.google.gson.Gson;
import com.ufpr.farmaalg.request.sandbox.SandboxRequest;
import com.ufpr.farmaalg.response.sandbox.comparams.SandboxComParamsResponse;
import com.ufpr.farmaalg.response.sandbox.erro.SandboxComErroResponse;
import com.ufpr.farmaalg.response.sandbox.semparams.SandboxSemParamsResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * @author Lia Alflen
 */

@Service
public class SandboxService {

    @Value("${farmaalg.sandbox.url}")
    private String sandboxUrl;

    public String fazRequisicaoAoSandbox(SandboxRequest sandboxRequest) throws URISyntaxException, IOException, InterruptedException {
        Gson gson = new Gson();
        String jsonRequest = gson.toJson(sandboxRequest);

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(sandboxUrl))
                .headers("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(jsonRequest)).build();

        HttpResponse<String> response = HttpClient.newBuilder()
                .build().send(request, HttpResponse.BodyHandlers.ofString());

        String body = response.body();

        String output;
        if (body.contains("COMPLETED")) {
            if (sandboxRequest.getParams() != null) {
                SandboxComParamsResponse sandboxResponse = gson.fromJson(response.body(), SandboxComParamsResponse.class);
                System.out.println("Tem param: " + sandboxResponse);
                output = sandboxResponse.getResult().get(0).getOutput();
            }else {
                SandboxSemParamsResponse sandboxResponse = gson.fromJson(response.body(), SandboxSemParamsResponse.class);
                System.out.println("Não tem param: " + sandboxResponse);
                output = sandboxResponse.getResult().getOutput();
            }
        } else {
            SandboxComErroResponse sandboxResponse = gson.fromJson(response.body(), SandboxComErroResponse.class);
            System.out.println("Erro: " + sandboxResponse);
            output = sandboxResponse.getOutput();
        }

        return output;
    }
}
