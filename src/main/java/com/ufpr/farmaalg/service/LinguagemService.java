package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.model.Linguagem;
import com.ufpr.farmaalg.model.Resposta;
import com.ufpr.farmaalg.model.relacionamento.linguagem_exercicio.LinguagemExercicio;
import com.ufpr.farmaalg.repository.LinguagemExercicioRepository;
import com.ufpr.farmaalg.repository.LinguagemRepository;
import com.ufpr.farmaalg.request.linguagem.LinguagemAtualizacaoRequest;
import com.ufpr.farmaalg.request.linguagem.LinguagemCadastroRequest;
import com.ufpr.farmaalg.response.linguagem.LinguagemResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class LinguagemService {

    @Autowired
    private LinguagemRepository linguagemRepository;

    @Autowired
    private LinguagemExercicioRepository linguagemExercicioRepository;

    @Autowired
    private RespostaService respostaService;

    public List<LinguagemResponse> buscarTodasLinguagens() {
        List<Linguagem> linguagens = linguagemRepository.findAll();

        List<LinguagemResponse> response = new ArrayList<>();

        for (Linguagem linguagem : linguagens) {
            response.add(converteLinguagemParaResponse(linguagem));
        }
        return response;
    }

    public LinguagemResponse buscarLinguagemPorCodigo(Long codigo) {
        Optional<Linguagem> optionalLinguagem = buscarLinguagemDoBancoPorCodigo(codigo);

        if (optionalLinguagem.isPresent()) {
            return converteLinguagemParaResponse(optionalLinguagem.get());
        }else{
            return null;
        }
    }

    public LinguagemResponse cadastrarNovaLinguagem(LinguagemCadastroRequest linguagemCadastroRequest) {
        Linguagem linguagem = new Linguagem(null, linguagemCadastroRequest.getNome(), linguagemCadastroRequest.getCodigoSandbox(), linguagemCadastroRequest.getExemplo());

        return converteLinguagemParaResponse(linguagemRepository.save(linguagem));
    }

    public LinguagemResponse atualizarLinguagem(Long codigo, LinguagemAtualizacaoRequest linguagemAtualizacaoRequest) {
        Optional<Linguagem> optionalLinguagem = buscarLinguagemDoBancoPorCodigo(codigo);

        if (optionalLinguagem.isEmpty()) {
            return null;
        }

        Linguagem linguagemBanco = optionalLinguagem.get();

        Linguagem linguagem = new Linguagem(linguagemAtualizacaoRequest.getId(), linguagemAtualizacaoRequest.getNome(), linguagemAtualizacaoRequest.getCodigoSandbox(), linguagemAtualizacaoRequest.getExemplo());

        BeanUtils.copyProperties(linguagem, linguagemBanco, "id");

        return converteLinguagemParaResponse(linguagemRepository.save(linguagemBanco));
    }

    public boolean removerLinguagem(Long codigo) {
        Optional<Linguagem> optionalLinguagem = buscarLinguagemDoBancoPorCodigo(codigo);

        if (optionalLinguagem.isEmpty()) {
            return false;
        }

        //remove relacionamentos
        List<LinguagemExercicio> linguagemExercicios = linguagemExercicioRepository.findAllByLinguagem(optionalLinguagem.get());
        linguagemExercicios.forEach(tag -> removeRelacionamentoLinguagemExercicio(tag));

        List<Resposta> respostas = respostaService.buscarRespostasDeUmaLinguagem(optionalLinguagem.get());
        respostas.forEach(resposta -> respostaService.removerResposta(resposta.getId(), null));

        linguagemRepository.delete(optionalLinguagem.get());
        return true;
    }

    private LinguagemResponse converteLinguagemParaResponse(Linguagem linguagem){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(linguagem, LinguagemResponse.class);
    }

    protected Optional<Linguagem> buscarLinguagemDoBancoPorCodigo(Long codigo) {
        return linguagemRepository.findById(codigo);
    }

    protected void removeRelacionamentoLinguagemExercicio(LinguagemExercicio linguagemExercicio) {
        linguagemExercicioRepository.delete(linguagemExercicio);
    }

    protected void removeRelacionamentosLinguagemExercicio(List<LinguagemExercicio> linguagemExercicios) {
        linguagemExercicioRepository.deleteAll(linguagemExercicios);
    }
}
