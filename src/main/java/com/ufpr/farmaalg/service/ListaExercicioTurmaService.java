package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.model.ListaExercicioTurma;
import com.ufpr.farmaalg.model.Turma;
import com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma.ListaExercicio;
import com.ufpr.farmaalg.repository.ListaExercicioRepository;
import com.ufpr.farmaalg.repository.ListaExercicioTurmaRepository;
import com.ufpr.farmaalg.request.IdRelacionamento;
import com.ufpr.farmaalg.request.listaexercicio.ListaExercicioAtualizacaoRequest;
import com.ufpr.farmaalg.request.listaexercicio.ListaExercicioCadastroRequest;
import com.ufpr.farmaalg.response.listaexercicio.ExercicioResponseSimplificado;
import com.ufpr.farmaalg.response.listaexercicio.ListaExercicioTurmaResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Lia Alflen
 */

@Service
public class ListaExercicioTurmaService {

    @Autowired
    private ListaExercicioTurmaRepository listaExercicioTurmaRepository;

    @Autowired
    private TurmaService turmaService;

    @Autowired
    private ListaExercicioRepository listaExercicioRepository;

    @Autowired
    private ExercicioService exercicioService;

    public List<ListaExercicioTurmaResponse> buscarTodasListaExerciciosTurma(Long codigoTurma) {
        Optional<Turma> optionalTurma = turmaService.buscarTurmaDoBancoPorCodigo(codigoTurma);

        if (optionalTurma.isEmpty()) {
            return null;
        }

        List<ListaExercicioTurma> listaExercicioTurmaList = listaExercicioTurmaRepository.findAllByTurma(optionalTurma.get());

//        List<ListaExercicioId> ids = listaExercicioTurmaList.stream().flatMap(map -> map.getListaExercicios().stream().map(ListaExercicio::getId)).collect(Collectors.toList());
//
//        List<ListaExercicio> listaExercicios = listaExercicioRepository.findAllByIdIn(ids);
//
//        for (ListaExercicioTurma listaExercicioTurma : listaExercicioTurmaList) {
//            List<ListaExercicio> listaExercicioList = new ArrayList<>();
//            listaExercicioList = listaExercicios.stream().filter(listaExercicio -> listaExercicioTurma.getListaExercicios().contains(listaExercicio)).collect(Collectors.toList());
////            listaExercicios.forEach(//
////                    listaExercicio -> {
////                        if(listaExercicioTurma.getListaExercicios().contains(listaExercicio)) {
////                            listaExercicioList.add();
////                        }
////                    }
////            );
//            listaExercicioTurma.setListaExercicios(listaExercicioList);
//        }

        List<ListaExercicioTurmaResponse> response = new ArrayList<>();

        for (ListaExercicioTurma listaExercicioTurma : listaExercicioTurmaList) {
            response.add(converteListaExercicioTurmaParaResponse(listaExercicioTurma));
        }

        return response;
    }

    public ListaExercicioTurmaResponse buscarListaExercicioTurmaPorCodigo(Long codigo) {
        Optional<ListaExercicioTurma> optionalListaExercicioTurma = buscarListaExercicioTurmaDoBancoPorCodigo(codigo);

        if (optionalListaExercicioTurma.isPresent()) {
            return converteListaExercicioTurmaParaResponse(optionalListaExercicioTurma.get());
        }else{
            return null;
        }
    }

    @Transactional
    public ListaExercicioTurmaResponse cadastrarNovaListaExercicioTurma(ListaExercicioCadastroRequest listaExercicioCadastroRequest) {
        Optional<Turma> optionalTurma = turmaService.buscarTurmaDoBancoPorCodigo(listaExercicioCadastroRequest.getTurma().getId());

        if (optionalTurma.isEmpty()) {
            throw new RuntimeException("Forçando rollback da transação");
        }

        ListaExercicioTurma listaExercicioTurma = new ListaExercicioTurma(null, listaExercicioCadastroRequest.getTitulo(), listaExercicioCadastroRequest.getDescricao(), listaExercicioCadastroRequest.getDataEntrega(), optionalTurma.get(), null);
        listaExercicioTurma = listaExercicioTurmaRepository.save(listaExercicioTurma);

        List<ExercicioResponseSimplificado> exercicios = new ArrayList<>();
        if (listaExercicioCadastroRequest.getExercicios() != null) {
            for (IdRelacionamento exercicio : listaExercicioCadastroRequest.getExercicios()) {
                ListaExercicio listaExercicio = exercicioService.adicionaRelacionamentoExercicioALista(exercicio.getId(), listaExercicioTurma);

                if (listaExercicio == null) {
                    throw new RuntimeException("Forçando rollback da transação");
                }

                exercicios.add(new ExercicioResponseSimplificado(listaExercicio.getExercicio().getId(), listaExercicio.getExercicio().getTitulo(), listaExercicio.getExercicio().getDescricao(),
                        listaExercicio.getExercicio().getEnunciado(), listaExercicio.getExercicio().isEhPublico(),
                        new IdRelacionamento(listaExercicio.getExercicio().getExercicioOriginal()==null?null:listaExercicio.getExercicio().getExercicioOriginal().getId())));
            }
        }

        ListaExercicioTurmaResponse listaExercicioTurmaResponse = new ListaExercicioTurmaResponse(listaExercicioTurma.getId(), listaExercicioTurma.getTitulo(), listaExercicioTurma.getDescricao(), listaExercicioTurma.getDataEntrega(), turmaService.converteTurmaParaResponse(listaExercicioTurma.getTurma()), null);

        listaExercicioTurmaResponse.setExercicios(exercicios);

        return listaExercicioTurmaResponse;
    }

    public ListaExercicioTurmaResponse atualizarListaExercicioTurma(Long codigo, ListaExercicioAtualizacaoRequest listaExercicioAtualizacaoRequest) {
        Optional<ListaExercicioTurma> optionalListaExercicioTurma = buscarListaExercicioTurmaDoBancoPorCodigo(codigo);

        if (optionalListaExercicioTurma.isEmpty()) {
            return null;
        }

        ListaExercicioTurma listaExercicioTurmaBanco = optionalListaExercicioTurma.get();

        Optional<Turma> optionalTurma = turmaService.buscarTurmaDoBancoPorCodigo(listaExercicioAtualizacaoRequest.getTurma().getId());

        if (optionalTurma.isEmpty()) {
            return null;
        }

        ListaExercicioTurma listaExercicioTurma = new ListaExercicioTurma(listaExercicioAtualizacaoRequest.getId(), listaExercicioAtualizacaoRequest.getTitulo(), listaExercicioAtualizacaoRequest.getDescricao(), listaExercicioAtualizacaoRequest.getDataEntrega(), optionalTurma.get(), null);

        BeanUtils.copyProperties(listaExercicioTurma, listaExercicioTurmaBanco, "id", "listaExercicios");

        return converteListaExercicioTurmaParaResponse(listaExercicioTurmaRepository.save(listaExercicioTurmaBanco));
    }

    @Transactional
    public boolean removerListaExercicioTurma(Long codigo) {
        Optional<ListaExercicioTurma> optionalListaExercicioTurma = buscarListaExercicioTurmaDoBancoPorCodigo(codigo);

        if (optionalListaExercicioTurma.isEmpty()) {
            return false;
        }

        List<ListaExercicio> listaExercicios = listaExercicioRepository.findByListaExercicioTurma_Id(optionalListaExercicioTurma.get().getId());
        listaExercicioRepository.deleteAll(listaExercicios);

        listaExercicioTurmaRepository.delete(optionalListaExercicioTurma.get());

        return true;
    }

    private ListaExercicioTurmaResponse converteListaExercicioTurmaParaResponse(ListaExercicioTurma listaExercicioTurma){
        ModelMapper modelMapper = new ModelMapper();

        List<ExercicioResponseSimplificado> exercicioResponse = new ArrayList<>();
        listaExercicioTurma.getListaExercicios().forEach(exercicio -> exercicioResponse.add(modelMapper.map(exercicio.getExercicio(),ExercicioResponseSimplificado.class)));

        ListaExercicioTurmaResponse response = modelMapper.map(listaExercicioTurma, ListaExercicioTurmaResponse.class);
        response.setExercicios(exercicioResponse);
        return response;
    }

    protected Optional<ListaExercicioTurma> buscarListaExercicioTurmaDoBancoPorCodigo(Long codigo) {
        return listaExercicioTurmaRepository.findById(codigo);
    }

}
