package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.config.security.AuthenticationFilter;
import com.ufpr.farmaalg.model.*;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurma;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurmaId;
import com.ufpr.farmaalg.model.relacionamento.linguagem_exercicio.LinguagemExercicio;
import com.ufpr.farmaalg.model.relacionamento.linguagem_exercicio.LinguagemExercicioId;
import com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma.ListaExercicio;
import com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma.ListaExercicioId;
import com.ufpr.farmaalg.model.relacionamento.tag_exercicio.TagExercicio;
import com.ufpr.farmaalg.model.relacionamento.tag_exercicio.TagExercicioId;
import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.repository.ExercicioRepository;
import com.ufpr.farmaalg.repository.LinguagemExercicioRepository;
import com.ufpr.farmaalg.repository.ListaExercicioRepository;
import com.ufpr.farmaalg.repository.TagExercicioRepository;
import com.ufpr.farmaalg.request.IdRelacionamento;
import com.ufpr.farmaalg.request.exercicio.*;
import com.ufpr.farmaalg.request.tag.TagCadastroRequest;
import com.ufpr.farmaalg.response.exercicio.DicaRelacionamento;
import com.ufpr.farmaalg.response.exercicio.ExercicioResponse;
import com.ufpr.farmaalg.response.linguagem.LinguagemResponse;
import com.ufpr.farmaalg.response.tag.TagResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Lia Alflen
 */

@Service
public class ExercicioService {

    @Autowired
    private ExercicioRepository exercicioRepository;

    @Autowired
    private TurmaService turmaService;

    @Autowired
    private ListaExercicioTurmaService listaExercicioTurmaService;

    @Autowired
    private ListaExercicioRepository listaExercicioRepository;

    @Autowired
    private DicaExercicioService dicaExercicioService;

    @Autowired
    private TagService tagService;

    @Autowired
    private TagExercicioRepository tagExercicioRepository;

    @Autowired
    private LinguagemService linguagemService;

    @Autowired
    private LinguagemExercicioRepository linguagemExercicioRepository;

    @Autowired
    private RespostaService respostaService;

    @Autowired
    private CasoTesteService casoTesteService;

    @Autowired
    private UsuarioService usuarioService;

    public List<ExercicioResponse> buscarTodosExerciciosPublicos(String token) {
        List<Exercicio> exercicios = new ArrayList<>();

        //professor pode visualizar apenas exercicios públicos da sua instituição e seus próprios
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            Optional<Usuario> optionalUsuarioLogado = usuarioService.buscarUsuarioDoBancoPorCodigo(idLogado);

            if (optionalUsuarioLogado.isEmpty()) {
                return null;
            }

            exercicios = exercicioRepository.findAllByEhPublicoAndTurma_Professor_Instituicao_IdOrTurma_Professor_Id(true, optionalUsuarioLogado.get().getInstituicao().getId(), idLogado);
        //admin pode visualizar todos os exercícios privados e públicos
        }else {
            exercicios = exercicioRepository.findAll();
        }

        List<ExercicioResponse> response = new ArrayList<>();

        for (Exercicio exercicio : exercicios) {
            response.add(converteExercicioParaResponse(exercicio));
        }
        return response;
    }

    public ExercicioResponse buscarExercicioPorCodigo(Long codigo, String token) {
        Optional<Exercicio> optionalExercicio;

        //aluno só pode buscar exercícios de turmas em que esteja cadastrado.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            List<AlunoTurma> turmasAluno = usuarioService.buscarTodasTurmasDeUmAluno(idLogado);

            List<Long> idsTurmas = turmasAluno.stream().map(AlunoTurma::getId).map(AlunoTurmaId::getIdTurma).collect(Collectors.toList());

            optionalExercicio = exercicioRepository.findByTurma_IdInAndId(idsTurmas, codigo);
        } else {
            optionalExercicio = buscarExercicioDoBancoPorCodigo(codigo);
        }

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        Exercicio exercicio = optionalExercicio.get();

        //professor só pode buscar exercício de sua instituição
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            Optional<Usuario> optionalUsuarioLogado = usuarioService.buscarUsuarioDoBancoPorCodigo(idLogado);

            if (optionalUsuarioLogado.isEmpty()) {
                return null;
            }

            if (!exercicio.getTurma().getProfessor().getInstituicao().equals(optionalUsuarioLogado.get().getInstituicao())) {
                return null;
            }
        }

        return converteExercicioParaResponse(exercicio);
    }

    public ExercicioResponse cadastrarNovoExercicio(ExercicioCadastroRequest exercicioCadastroRequest, String token) {
        ModelMapper modelMapper = new ModelMapper();
        Exercicio exercicio = modelMapper.map(exercicioCadastroRequest, Exercicio.class);

        exercicio = verificaTurma(exercicio, exercicioCadastroRequest.getTurma());
        if (exercicio == null) return null;

        //Se for professor, verifica se está inserindo exercício nas suas turmas.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!exercicio.getTurma().getProfessor().getId().equals(idLogado)) {
                return null;
            }
        }

        ExercicioResponse exercicioResponse = converteExercicioParaResponse(exercicioRepository.save(exercicio));
        exercicioResponse.setLinguagens(new ArrayList<>());
        exercicioResponse.setDicas(new ArrayList<>());
        exercicioResponse.setTags(new ArrayList<>());

        return exercicioResponse;
    }

    @Transactional
    public ExercicioResponse cadastrarNovoExercicioCompleto(ExercicioCompletoCadastroRequest exercicioCadastroCompletoRequest, String token) {

        ModelMapper modelMapper = new ModelMapper();
        Exercicio exercicio = modelMapper.map(exercicioCadastroCompletoRequest, Exercicio.class);

        exercicio = verificaTurma(exercicio, exercicioCadastroCompletoRequest.getTurma());
        if (exercicio == null) throw new RuntimeException("Forçando rollback da transação");

        //Se for professor, verifica se está inserindo exercício nas suas turmas.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!exercicio.getTurma().getProfessor().getId().equals(idLogado)) {
                throw new RuntimeException("Forçando rollback da transação");
            }
        }

        exercicio = exercicioRepository.save(exercicio);

        List<LinguagemResponse> linguagens = new ArrayList<>();
        if (exercicioCadastroCompletoRequest.getLinguagens() != null) {
            for (IdRelacionamento idLinguagem : exercicioCadastroCompletoRequest.getLinguagens()) {
                LinguagemExercicio linguagemExercicio = adicionaRelacionamentoLinguagemExercicio(idLinguagem.getId(), exercicio);
                if (linguagemExercicio == null) throw new RuntimeException("Forçando rollback da transação");
                ;
                linguagens.add(new LinguagemResponse(linguagemExercicio.getLinguagem().getId(), linguagemExercicio.getLinguagem().getNome(), linguagemExercicio.getLinguagem().getCodigoSandbox(), linguagemExercicio.getLinguagem().getExemplo()));
            }
        }

        List<TagResponse> tags = new ArrayList<>();
        if (exercicioCadastroCompletoRequest.getTags() != null) {
            for (TagCadastroRequest nomeTag : exercicioCadastroCompletoRequest.getTags()) {
                TagExercicio tagExercicio = adicionaRelacionamentoTagExercicio(nomeTag.getNome(), exercicio);
                tags.add(new TagResponse(tagExercicio.getTag().getId(), tagExercicio.getTag().getNome()));
            }
        }

        List<DicaRelacionamento> dicas = new ArrayList<>();
        if (exercicioCadastroCompletoRequest.getDicas() != null) {
            for (DicaRelacionamentoRequest dica : exercicioCadastroCompletoRequest.getDicas()) {
                DicaExercicio dicaExercicio = modelMapper.map(dica, DicaExercicio.class);
                dicaExercicio.setId(null);
                dicaExercicio.setExercicio(exercicio);

                dicaExercicio = dicaExercicioService.cadastraDicaExercicioNoBanco(dicaExercicio);

                dicas.add(new DicaRelacionamento(dicaExercicio.getId(), dicaExercicio.getTitulo(), dicaExercicio.getDescricao(), dicaExercicio.getNumeroTentativas()));
            }
        }

        ExercicioResponse exercicioResponse = new ExercicioResponse(exercicio.getId(), exercicio.getTitulo(), exercicio.getDescricao(), exercicio.getEnunciado(), exercicio.isEhPublico(), turmaService.converteTurmaParaResponse(exercicio.getTurma()), exercicio.getExercicioOriginal() == null ? null:new IdRelacionamento(exercicio.getExercicioOriginal().getId()), null, null, null);
        exercicioResponse.setLinguagens(linguagens);
        exercicioResponse.setTags(tags);
        exercicioResponse.setDicas(dicas);

        return exercicioResponse;
    }

    @Transactional
    public ExercicioResponse atualizarExercicio(Long codigo, ExercicioAtualizacaoRequest exercicioAtualizacaoRequest, String token) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigo);

        if (optionalExercicio.isEmpty()) {
            throw new RuntimeException("Forçando rollback da transação 1");
        }

        Exercicio exercicioBanco = optionalExercicio.get();

        //Se for professor, verifica se está atualizando um exercício que pertence a ele.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!exercicioBanco.getTurma().getProfessor().getId().equals(idLogado)) {
                throw new RuntimeException("Forçando rollback da transação 2");
            }
        }

        BeanUtils.copyProperties(exercicioAtualizacaoRequest, exercicioBanco, "id", "exercicioOriginal", "tagsExercicio", "linguagensExercicio", "dicas");

        exercicioBanco = verificaTurma(exercicioBanco, exercicioAtualizacaoRequest.getTurma());

        if (exercicioBanco == null) {
            throw new RuntimeException("Forçando rollback da transação 3");
        }

        tagService.removeRelacionamentosTagExercicio(exercicioBanco.getTagsExercicio());
        linguagemService.removeRelacionamentosLinguagemExercicio(exercicioBanco.getLinguagensExercicio());
        dicaExercicioService.RemoverListaDeDicaExercicio(exercicioBanco.getDicas());

        exercicioBanco.setTagsExercicio(new ArrayList<>());
        if (exercicioAtualizacaoRequest.getTags()!= null) {
            for (TagCadastroRequest tag : exercicioAtualizacaoRequest.getTags()) {
                TagExercicio tagExercicio = adicionaRelacionamentoTagExercicio(tag.getNome(), exercicioBanco);
                exercicioBanco.getTagsExercicio().add(tagExercicio);
            }
        }

        exercicioBanco.setLinguagensExercicio(new ArrayList<>());
        if (exercicioAtualizacaoRequest.getLinguagens()!= null) {
            for (IdRelacionamento idLinguagem : exercicioAtualizacaoRequest.getLinguagens()) {
                LinguagemExercicio linguagemExercicio = adicionaRelacionamentoLinguagemExercicio(idLinguagem.getId(), exercicioBanco);
                if (linguagemExercicio == null) throw new RuntimeException("Forçando rollback da transação 4");
                ;
                exercicioBanco.getLinguagensExercicio().add(linguagemExercicio);
            }
        }

        exercicioBanco.setDicas(new ArrayList<>());
        if (exercicioAtualizacaoRequest.getDicas()!= null) {
            for (DicaRelacionamentoRequest dica : exercicioAtualizacaoRequest.getDicas()) {
                DicaExercicio dicaExercicio = dicaExercicioService.criaECadastraDicaExercicio(exercicioBanco, dica.getTitulo(), dica.getDescricao(), dica.getNumeroTentativas());
                exercicioBanco.getDicas().add(dicaExercicio);
            }
        }

        return converteExercicioParaResponse(exercicioRepository.save(exercicioBanco));
    }

    @Transactional
    public boolean removerExercicio(Long codigo, String token) {

        if (!removeExercicioERelacionamentos(codigo, token)) return false;
        return true;
    }

    protected boolean removeExercicioERelacionamentos(Long codigo, String token) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigo);

        if (optionalExercicio.isEmpty()) {
            return false;
        }

        if (token != null) {
            //Se for professor, verifica que está excluindo um exercício que pertence a ele.
            if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
                Long idLogado = AuthenticationFilter.getIdFromToken(token);

                if (!optionalExercicio.get().getTurma().getProfessor().getId().equals(idLogado)) {
                    return false;
                }
            }
        }

        List<ListaExercicio> listaExercicios = listaExercicioRepository.findByExercicio_Id(optionalExercicio.get().getId());
        listaExercicioRepository.deleteAll(listaExercicios);

        SetaNullExerciciosQueReferenciamOsSendoRemovidos(optionalExercicio.get());

        List<DicaExercicio> dicasExercico = dicaExercicioService.findDicaExercicioByExercicio(optionalExercicio.get());
        dicaExercicioService.RemoverListaDeDicaExercicio(dicasExercico);

        List<TagExercicio> tagExercicios = tagExercicioRepository.findAllByExercicio(optionalExercicio.get());
        tagExercicios.forEach(tag -> tagService.removeRelacionamentoTagExercicio(tag));

        List<LinguagemExercicio> linguagemExercicios = linguagemExercicioRepository.findAllByExercicio(optionalExercicio.get());
        linguagemExercicios.forEach(tag -> linguagemService.removeRelacionamentoLinguagemExercicio(tag));

        List<Resposta> respostas = respostaService.buscarRespostasDeUmExercicio(optionalExercicio.get());
        respostas.forEach(resposta -> respostaService.removerResposta(resposta.getId(), null));

        List<CasoTeste> casosTeste = casoTesteService.buscarTodosCasosTestePorExercicio(optionalExercicio.get());
        casosTeste.forEach(casoTeste -> casoTesteService.removerCasoTeste(casoTeste.getId(), null));

        exercicioRepository.delete(optionalExercicio.get());
        return true;
    }

    @Transactional
    public ExercicioResponse cadastraExercicioParaOutroProfessor(Long codigoExercicio, Long codigoTurma) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty() || !optionalExercicio.get().isEhPublico()) {
            throw new RuntimeException("Forçando rollback da transação");
        }

        Exercicio exercicioBanco = optionalExercicio.get();

        //só permite copiar exercícios públicos
        if (!exercicioBanco.isEhPublico()) {
            throw new RuntimeException("Forçando rollback da transação");
        }

        Optional<Turma> optionalTurma = turmaService.buscarTurmaDoBancoPorCodigo(codigoTurma);

        if (optionalTurma.isEmpty() || exercicioBanco.getTurma().getId().equals(optionalTurma.get().getId())) {
            throw new RuntimeException("Forçando rollback da transação");
        }

        Turma turmaBanco = optionalTurma.get();

        //só permite copiar exercícios de uma mesma instituição
        if (!turmaBanco.getProfessor().getInstituicao().equals(exercicioBanco.getTurma().getProfessor().getInstituicao())) {
            throw new RuntimeException("Forçando rollback da transação");
        }

        Exercicio exercicioOriginal = exercicioBanco;

        if (exercicioBanco.getExercicioOriginal() != null) {
            exercicioOriginal = exercicioBanco.getExercicioOriginal();
        }

        Exercicio novoExercicio = new Exercicio(turmaBanco, exercicioOriginal, exercicioBanco.getTitulo(), exercicioBanco.getDescricao(), exercicioBanco.getEnunciado(), exercicioBanco.isEhPublico());
        novoExercicio = exercicioRepository.save(novoExercicio);

        novoExercicio.setDicas(new ArrayList<>());
        if (exercicioBanco.getDicas()!=null) {
            for (DicaExercicio dica : exercicioBanco.getDicas()) {
                DicaExercicio dicaExercicio = new DicaExercicio(null, novoExercicio, dica.getTitulo(), dica.getDescricao(), dica.getNumeroTentativas());
                novoExercicio.getDicas().add(dicaExercicioService.cadastraDicaExercicioNoBanco(dicaExercicio));
            }
        }

        novoExercicio.setLinguagensExercicio(new ArrayList<>());
        if (exercicioBanco.getLinguagensExercicio()!=null) {
            for (LinguagemExercicio linguagemExercicio : exercicioBanco.getLinguagensExercicio()) {
                LinguagemExercicio linguagemExercicioCriada = adicionaRelacionamentoLinguagemExercicio(linguagemExercicio.getLinguagem().getId(), novoExercicio);
                if (linguagemExercicioCriada == null) throw new RuntimeException("Forçando rollback da transação 4");
                novoExercicio.getLinguagensExercicio().add(linguagemExercicioCriada);
            }
        }

        novoExercicio.setTagsExercicio(new ArrayList<>());
        if (exercicioBanco.getTagsExercicio()!=null) {
            for (TagExercicio tagExercicio: exercicioBanco.getTagsExercicio()) {
                TagExercicio tagExercicioCriada = adicionaRelacionamentoTagExercicio(tagExercicio.getTag().getNome(), novoExercicio);
                novoExercicio.getTagsExercicio().add(tagExercicioCriada);
            }
        }

        return converteExercicioParaResponse(novoExercicio);
    }

    public boolean adicionarExercicioAUmaLista(Long codigoExercicio, Long codigoLista, String token) {
        Optional<ListaExercicioTurma> optionalListaExercicioTurma = listaExercicioTurmaService.buscarListaExercicioTurmaDoBancoPorCodigo(codigoLista);

        if (optionalListaExercicioTurma.isEmpty()) {
            return false;
        }

        ListaExercicioTurma listaExercicioTurma = optionalListaExercicioTurma.get();

        //Se for professor, verifica que está adicionando um exercício a uma lista que pertence a ele.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!listaExercicioTurma.getTurma().getProfessor().getId().equals(idLogado)) {
                return false;
            }
        }

        if (adicionaRelacionamentoExercicioALista(codigoExercicio, listaExercicioTurma) == null) return false;

        return true;
    }

    public boolean removerExercicioDeUmaLista(Long codigoExercicio, Long codigoLista, String token) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return false;
        }

        Optional<ListaExercicioTurma> optionalListaExercicioTurma = listaExercicioTurmaService.buscarListaExercicioTurmaDoBancoPorCodigo(codigoLista);

        if (optionalListaExercicioTurma.isEmpty()) {
            return false;
        }

        Exercicio exercicio = optionalExercicio.get();
        ListaExercicioTurma listaExercicioTurma = optionalListaExercicioTurma.get();

        //Se for professor, verifica que está removendo um exercício de uma lista que pertence a ele.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!listaExercicioTurma.getTurma().getProfessor().getId().equals(idLogado)) {
                return false;
            }
        }

        ListaExercicioId listaExercicioId = new ListaExercicioId(listaExercicioTurma.getId(), exercicio.getId());
        ListaExercicio listaExercicio = new ListaExercicio(listaExercicioId, listaExercicioTurma, exercicio);

        //futuramente verificar se exercicio está presente na lista

        listaExercicioRepository.delete(listaExercicio);

        return true;
    }

    public boolean adicionarTagAUmExercicio(Long codigoExercicio, String nomeTag, String token) {
        //futuramente ver se é interessante adicionar codigoTag
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return false;
        }

        Exercicio exercicio = optionalExercicio.get();

        //Se for professor, verifica que está adicionando tag a um exercício que pertence a ele.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!exercicio.getTurma().getProfessor().getId().equals(idLogado)) {
                return false;
            }
        }

        adicionaRelacionamentoTagExercicio(nomeTag, exercicio);

        return true;
    }

    private TagExercicio adicionaRelacionamentoTagExercicio(String nomeTag, Exercicio exercicio) {
        Optional<Tag> optionalTag = tagService.buscarTagPorNome(nomeTag);

        Tag tag = new Tag(null, nomeTag);

        if (optionalTag.isEmpty()) {
            tag = tagService.salvarTagNoBanco(tag);
        } else {
            tag.setId(optionalTag.get().getId());
        }

        TagExercicioId tagExercicioId = new TagExercicioId(tag.getId(), exercicio.getId());
        TagExercicio tagExercicio = new TagExercicio(tagExercicioId, tag, exercicio);

        return tagExercicioRepository.save(tagExercicio);
    }

    public boolean removerTagDeUmExercicio(Long codigoExercicio, Long codigoTag, String token) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return false;
        }

        Optional<Tag> optionalTag = tagService.buscarTagDoBancoPorCodigo(codigoTag);

        if (optionalTag.isEmpty()) {
            return false;
        }

        Exercicio exercicio = optionalExercicio.get();
        Tag tag = optionalTag.get();

        //Se for professor, verifica que está removendo tag de um exercício que pertence a ele.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!exercicio.getTurma().getProfessor().getId().equals(idLogado)) {
                return false;
            }
        }

        TagExercicioId tagExercicioId = new TagExercicioId(tag.getId(), exercicio.getId());
        TagExercicio tagExercicio = new TagExercicio(tagExercicioId, tag, exercicio);

        //futuramente verificar se tag está presente no exercicio
        tagExercicioRepository.delete(tagExercicio);

        //Todo: verificar caso tag não esteja mais presente em nenhum exercicio, exclui-la.

        return true;
    }

    public boolean adicionarLinguagemAUmExercicio(Long codigoExercicio, Long codigoLinguagem, String token) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return false;
        }

        Exercicio exercicio = optionalExercicio.get();

        //Se for professor, verifica que está adicionando linguagem a um exercício que pertence a ele.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!exercicio.getTurma().getProfessor().getId().equals(idLogado)) {
                return false;
            }
        }

        if (adicionaRelacionamentoLinguagemExercicio(codigoLinguagem, exercicio) == null) return false;

        return true;
    }

    private LinguagemExercicio adicionaRelacionamentoLinguagemExercicio(Long codigoLinguagem, Exercicio exercicio) {
        Optional<Linguagem> optionalLinguagem = linguagemService.buscarLinguagemDoBancoPorCodigo(codigoLinguagem);

        if (optionalLinguagem.isEmpty()) {
            return null;
        }

        Linguagem linguagem = optionalLinguagem.get();

        LinguagemExercicioId linguagemExercicioId = new LinguagemExercicioId(linguagem.getId(), exercicio.getId());
        LinguagemExercicio linguagemExercicio = new LinguagemExercicio(linguagemExercicioId, linguagem, exercicio);

        return linguagemExercicioRepository.save(linguagemExercicio);
    }

    public boolean removerLinguagemDeUmExercicio(Long codigoExercicio, Long codigoLinguagem, String token) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return false;
        }

        Optional<Linguagem> optionalLinguagem = linguagemService.buscarLinguagemDoBancoPorCodigo(codigoLinguagem);

        if (optionalLinguagem.isEmpty()) {
            return false;
        }

        Exercicio exercicio = optionalExercicio.get();
        Linguagem linguagem = optionalLinguagem.get();

        //Se for professor, verifica que está removendo linguagem de um exercício que pertence a ele.
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!exercicio.getTurma().getProfessor().getId().equals(idLogado)) {
                return false;
            }
        }

        LinguagemExercicioId linguagemExercicioId = new LinguagemExercicioId(linguagem.getId(), exercicio.getId());
        LinguagemExercicio linguagemExercicio = new LinguagemExercicio(linguagemExercicioId, linguagem, exercicio);

        //futuramente verificar se tag está presente no exercicio
        linguagemExercicioRepository.delete(linguagemExercicio);

        return true;
    }

    protected ExercicioResponse converteExercicioParaResponse(Exercicio exercicio) {
        ModelMapper modelMapper = new ModelMapper();

        List<LinguagemResponse> linguagens = new ArrayList<>();

        if (exercicio.getLinguagensExercicio()!=null){
            exercicio.getLinguagensExercicio().forEach(linguagemExercicio ->{
                    linguagens.add(new LinguagemResponse(linguagemExercicio.getLinguagem().getId(), linguagemExercicio.getLinguagem().getNome(), linguagemExercicio.getLinguagem().getCodigoSandbox(), linguagemExercicio.getLinguagem().getExemplo()));
            });
        }

        List<TagResponse> tags = new ArrayList<>();
        if (exercicio.getTagsExercicio()!=null) {
            exercicio.getTagsExercicio().forEach(tagExercicio -> {
                tags.add(new TagResponse(tagExercicio.getTag().getId(), tagExercicio.getTag().getNome()));
            });
        }

        exercicio.setLinguagensExercicio(null);
        exercicio.setTagsExercicio(null);

        ExercicioResponse response = modelMapper.map(exercicio, ExercicioResponse.class);
        response.setLinguagens(linguagens);
        response.setTags(tags);

        return response;
    }

    private Exercicio verificaTurma(Exercicio exercicio, IdRelacionamento turma) {
        Optional<Turma> optionalTurma = turmaService.buscarTurmaDoBancoPorCodigo(turma.getId());

        if (optionalTurma.isEmpty()) {
            return null;
        }

        exercicio.setTurma(optionalTurma.get());

        return exercicio;
    }

    protected Optional<Exercicio> buscarExercicioDoBancoPorCodigo(Long codigo) {
        return exercicioRepository.findById(codigo);
    }

    private void SetaNullExerciciosQueReferenciamOsSendoRemovidos(Exercicio exercicio) {
        List<Exercicio> exerciciosQueReferenciamExercicioSendoExcluido = buscarExercicioPorExercicioOriginalId(Arrays.asList((exercicio.getId())));
        exerciciosQueReferenciamExercicioSendoExcluido.forEach(ex -> ex.setExercicioOriginal(null));

        atualizarExercicios(exerciciosQueReferenciamExercicioSendoExcluido);
    }

    protected List<Exercicio> buscarExercicioPorExercicioOriginalId(List<Long> ids) {
        return exercicioRepository.findAllByExercicioOriginalIdIn(ids);
    }

    protected void atualizarExercicios(List<Exercicio> exercicios) {
        exercicioRepository.saveAll(exercicios);
    }

    protected List<Exercicio> listaTodosExerciciosPorTurma(Turma turma) {
        return exercicioRepository.findAllByTurma(turma);
    }

    protected boolean verificaSeExercicioPermiteLinguagem(Exercicio exercicio, Linguagem linguagem) {
        LinguagemExercicio linguagemExercicio = linguagemExercicioRepository.findByExercicioAndLinguagem(exercicio, linguagem);

        if (linguagemExercicio == null) {
            return false;
        }
        return true;
    }

    protected ListaExercicio adicionaRelacionamentoExercicioALista(Long codigoExercicio, ListaExercicioTurma listaExercicioTurma) {
        Optional<Exercicio> optionalExercicio = buscarExercicioDoBancoPorCodigo(codigoExercicio);

        if (optionalExercicio.isEmpty()) {
            return null;
        }

        Exercicio exercicio = optionalExercicio.get();

        if (!exercicio.getTurma().equals(listaExercicioTurma.getTurma())) {
            return null;
        }

        ListaExercicioId listaExercicioId = new ListaExercicioId(listaExercicioTurma.getId(), exercicio.getId());
        ListaExercicio listaExercicio = new ListaExercicio(listaExercicioId, listaExercicioTurma, exercicio);

        return listaExercicioRepository.save(listaExercicio);
    }
}
