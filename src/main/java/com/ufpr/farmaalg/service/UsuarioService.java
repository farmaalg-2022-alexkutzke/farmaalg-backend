package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.config.security.AuthenticationFilter;
import com.ufpr.farmaalg.model.Instituicao;
import com.ufpr.farmaalg.model.Resposta;
import com.ufpr.farmaalg.model.Usuario;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurma;
import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.repository.AlunoTurmaRepository;
import com.ufpr.farmaalg.repository.UsuarioRepository;
import com.ufpr.farmaalg.request.usuario.AdminAtualizacaoSenhaRequest;
import com.ufpr.farmaalg.request.usuario.UsuarioAtualizacaoRequest;
import com.ufpr.farmaalg.request.usuario.UsuarioAtualizacaoSenhaRequest;
import com.ufpr.farmaalg.request.usuario.UsuarioCadastroRequest;
import com.ufpr.farmaalg.response.usuario.UsuarioResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private AlunoTurmaRepository alunoTurmaRepository;

    @Autowired
    private TurmaService turmaService;

    @Autowired
    private RespostaService respostaService;

    @Autowired
    private InstituicaoService instituicaoService;

    @Value("${farmaalg.security.salt}")
    private String salt;

    public List<UsuarioResponse> buscarTodosUsuarios(String token) {
        List<Usuario> usuarios;

        //verifica se o perfil buscando é de um professor para buscar usuários apenas da instituição do professor
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            Optional<Usuario> optionalUsuarioLogado = buscarUsuarioDoBancoPorCodigo(idLogado);

            if (optionalUsuarioLogado.isEmpty()) {
                return null;
            }

            usuarios = usuarioRepository.findAllByInstituicao(optionalUsuarioLogado.get().getInstituicao());
        //se o perfil é admin, busca todos os usuários
        } else {
            usuarios = usuarioRepository.findAll();
        }

        List<UsuarioResponse> response = new ArrayList<>();

        for (Usuario usuario : usuarios) {
            response.add(converteUsuarioParaResponse(usuario));
        }
        return response;
    }

    public UsuarioResponse buscarUsuarioPorCodigo(Long codigo, String token) {
        Optional<Usuario> optionalUsuarioBuscado = buscarUsuarioDoBancoPorCodigo(codigo);

        //verifica se usuario sendo buscando existe
        if (optionalUsuarioBuscado.isEmpty()) {
            return null;
        }

        //verifica se perfil buscando é de um professor para ver se usuario sendo buscado está na mesma instituicao que o professor
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            Optional<Usuario> optionalUsuarioLogado = buscarUsuarioDoBancoPorCodigo(idLogado);

            if (optionalUsuarioLogado.isEmpty()) {
                return null;
            }

            if (!optionalUsuarioLogado.get().getInstituicao().getId().equals(optionalUsuarioBuscado.get().getInstituicao().getId())) {
                return null;
            }
        }

        return converteUsuarioParaResponse(optionalUsuarioBuscado.get());
    }

    public UsuarioResponse buscarUsuarioPorToken(String token) {

        Long idLogado = AuthenticationFilter.getIdFromToken(token);

        Optional<Usuario> optionalUsuarioLogado = buscarUsuarioDoBancoPorCodigo(idLogado);

        if (optionalUsuarioLogado.isEmpty()) {
                return null;
        }

        return converteUsuarioParaResponse(optionalUsuarioLogado.get());
    }

    public UsuarioResponse cadastrarNovoUsuario(UsuarioCadastroRequest usuarioCadastroRequest) {
        Usuario usuario = new Usuario(null, usuarioCadastroRequest.getPerfil(), null, null, usuarioCadastroRequest.getNome(), usuarioCadastroRequest.getEmail(), usuarioCadastroRequest.getIdInstitucional(), usuarioCadastroRequest.getTelefone(), usuarioCadastroRequest.getAvatar(), usuarioCadastroRequest.getSenha());

        if (verificaSeEmailEstaSendoUtilizado(usuario.getEmail())) {
            return null;
        }

        //Professor e aluno devem ter o atributo Instituição setado
        if(!usuarioCadastroRequest.getPerfil().equals(Perfil.ADMIN)){
            if (usuarioCadastroRequest.getInstituicao() == null || usuarioCadastroRequest.getInstituicao().getId() == null) {
                return null;
            }

            Optional<Instituicao> optionalInstituicao = instituicaoService.buscarInstituicaoDoBancoPorCodigo(usuarioCadastroRequest.getInstituicao().getId());

            if (optionalInstituicao.isEmpty()) {
                return null;
            }

            usuario.setInstituicao(optionalInstituicao.get());

            //IdInstitucional é obrigatório para o aluno
            if (usuarioCadastroRequest.getPerfil().equals(Perfil.ALUNO)) {
                if (usuario.getIdInstitucional() == null || usuario.getIdInstitucional().isEmpty()) {
                    return null;
                }
            }
        }

        usuario.setSenha(bCryptPasswordEncoder.encode(salt+usuario.getSenha()));

        return converteUsuarioParaResponse(usuarioRepository.save(usuario));
    }

    public UsuarioResponse atualizarUsuario(Long codigo, UsuarioAtualizacaoRequest usuarioAtualizacaoRequest, String token) {
        Optional<Usuario> optionalUsuario = buscarUsuarioDoBancoPorCodigo(codigo);

        if (optionalUsuario.isEmpty()) {
            return null;
        }

        //Professor e aluno podem atualizar apenas seu próprio cadastro
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)
                || Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!idLogado.equals(optionalUsuario.get().getId())) {
                return null;
            }
        }

        Usuario usuarioBanco = optionalUsuario.get();

        Instituicao instituicao = null;
        if (!usuarioBanco.getPerfil().equals(Perfil.ADMIN)) {
            if (usuarioAtualizacaoRequest.getInstituicao() == null || usuarioAtualizacaoRequest.getInstituicao().getId() == null) {
                return null;
            }

            Optional<Instituicao> optionalInstituicao = instituicaoService.buscarInstituicaoDoBancoPorCodigo(usuarioAtualizacaoRequest.getInstituicao().getId());

            if (optionalInstituicao.isEmpty()) {
                return null;
            }

            instituicao = optionalInstituicao.get();
        }

        ModelMapper modelMapper = new ModelMapper();
        Usuario usuario = modelMapper.map(usuarioAtualizacaoRequest, Usuario.class);

        usuario.setInstituicao(instituicao);

        //verifica disponibilidade de novo email
        if (!usuario.getEmail().equals(usuarioBanco.getEmail())) {
            if (verificaSeEmailEstaSendoUtilizado(usuario.getEmail())) {
                return null;
            }
        }

        BeanUtils.copyProperties(usuario, usuarioBanco, "id", "perfil", "senha");

        //Apenas o admin pode modificar o perfil de um usuário
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ADMIN)) {
            usuarioBanco.setPerfil(usuario.getPerfil());
        }

        return converteUsuarioParaResponse(usuarioRepository.save(usuarioBanco));
    }

    public boolean removerUsuario(Long codigo, String token) {
        //Professor e aluno podem remover apenas seu próprio cadastro
        if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)
                || Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.ALUNO)) {
            Long idLogado = AuthenticationFilter.getIdFromToken(token);

            if (!idLogado.equals(codigo)) {
                return false;
            }
        }

        return removeUsuario(codigo);
    }

    private UsuarioResponse converteUsuarioParaResponse(Usuario usuario){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(usuario, UsuarioResponse.class);
    }

    private boolean verificaSeEmailEstaSendoUtilizado(String email) {
        Usuario usuarioBanco = usuarioRepository.findByEmail(email);

        if (usuarioBanco != null) {
            return true;
        }
        return false;
    }

    protected boolean removeUsuario(Long codigo) {
        Optional<Usuario> optionalUsuario = buscarUsuarioDoBancoPorCodigo(codigo);

        if (optionalUsuario.isEmpty()) {
            return false;
        }

        Usuario usuario= optionalUsuario.get();

        //se for aluno, remove ele das turmas e suas respostas
        if (usuario.getPerfil().equals(Perfil.ALUNO)) {
            List<AlunoTurma> alunoTurmas = alunoTurmaRepository.findByAluno(optionalUsuario.get());
            alunoTurmaRepository.deleteAll(alunoTurmas);

            List<Resposta> respostas = respostaService.buscarRespostasDeUmAluno(optionalUsuario.get());
            respostas.forEach(resposta -> respostaService.removerResposta(resposta.getId(), null));

            //se for professor, remove suas turmas
        }else if (usuario.getPerfil().equals(Perfil.PROFESSOR)){
            if (usuario.getTurmasProfessor() != null){
                usuario.getTurmasProfessor().forEach(turma -> turmaService.removeTurmaERelacionamentosBuscandoTurma(turma.getId()));
            }
        }

        usuarioRepository.delete(usuario);
        return true;
    }

    protected Optional<Usuario> buscarUsuarioDoBancoPorCodigo(Long codigo) {
        return usuarioRepository.findById(codigo);
    }

    protected List<AlunoTurma> buscarTodasTurmasDeUmAluno(Long codigo) {
        return alunoTurmaRepository.findByAluno_Id(codigo);
    }

    public UsuarioResponse atualizarSenhaUsuario(UsuarioAtualizacaoSenhaRequest usuarioAtualizacaoSenhaRequest, String token) {
        Long idLogado = AuthenticationFilter.getIdFromToken(token);

        Optional<Usuario> optionalUsuario = buscarUsuarioDoBancoPorCodigo(idLogado);

        if (optionalUsuario.isEmpty()) {
            return null;
        }

        Usuario usuarioLogado = optionalUsuario.get();

        //verifica se senhaAntiga é igual a senha atual do usuário no banco
        if (!bCryptPasswordEncoder.matches(salt+usuarioAtualizacaoSenhaRequest.getSenhaAntiga(),usuarioLogado.getSenha())) {
            return null;
        }

        usuarioLogado.setSenha(bCryptPasswordEncoder.encode(salt+usuarioAtualizacaoSenhaRequest.getSenhaNova()));
        return converteUsuarioParaResponse(usuarioRepository.save(usuarioLogado));
    }

    public UsuarioResponse adminAtualizarSenhaUsuario(AdminAtualizacaoSenhaRequest adminAtualizacaoSenhaRequest) {
        Optional<Usuario> optionalUsuario = buscarUsuarioDoBancoPorCodigo(adminAtualizacaoSenhaRequest.getId());

        if (optionalUsuario.isEmpty()) {
            return null;
        }

        Usuario usuario = optionalUsuario.get();

        usuario.setSenha(bCryptPasswordEncoder.encode(salt+adminAtualizacaoSenhaRequest.getSenhaNova()));
        return converteUsuarioParaResponse(usuarioRepository.save(usuario));
    }
}
