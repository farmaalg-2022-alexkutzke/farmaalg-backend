package com.ufpr.farmaalg.service;

import com.ufpr.farmaalg.config.security.AuthenticationFilter;
import com.ufpr.farmaalg.model.Tag;
import com.ufpr.farmaalg.model.relacionamento.tag_exercicio.TagExercicio;
import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.repository.TagExercicioRepository;
import com.ufpr.farmaalg.request.tag.TagAtualizacaoRequest;
import com.ufpr.farmaalg.request.tag.TagCadastroRequest;
import com.ufpr.farmaalg.response.tag.ExercicioSemTagResponse;
import com.ufpr.farmaalg.response.tag.TagComExerciciosResponse;
import com.ufpr.farmaalg.response.tag.TagResponse;
import com.ufpr.farmaalg.repository.TagRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

@Service
public class TagService {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TagExercicioRepository tagExercicioRepository;

    @Autowired
    private ExercicioService exercicioService;

    public List<TagResponse> buscarTodasTags() {
        List<Tag> tags = tagRepository.findAll();

        List<TagResponse> response = new ArrayList<>();

        for (Tag tag : tags) {
            response.add(converteTagParaResponse(tag));
        }
        return response;
    }

    public TagComExerciciosResponse buscarTagPorCodigo(Long codigo, String token) {
        Optional<Tag> optionalTag = buscarTagDoBancoPorCodigo(codigo);

        if (optionalTag.isPresent()) {
            Tag tag = optionalTag.get();

            List<TagExercicio> tagExercicios = tagExercicioRepository.findAllByTag(tag);
            List<ExercicioSemTagResponse> exercicioResponses = new ArrayList<>();

            ModelMapper modelMapper = new ModelMapper();
            for (TagExercicio tagExercicio : tagExercicios) {
                //professor pode visualizar apenas exercicios públicos da sua instituição e seus próprios
                if (Perfil.valueOf(AuthenticationFilter.getRoleFromToken(token)).equals(Perfil.PROFESSOR)) {
                    Long idLogado = AuthenticationFilter.getIdFromToken(token);

                    if (tagExercicio.getExercicio().getTurma().getProfessor().getId().equals(idLogado)
                            ||tagExercicio.getExercicio().isEhPublico()) {
                        exercicioResponses.add(modelMapper.map(exercicioService.converteExercicioParaResponse(tagExercicio.getExercicio()), ExercicioSemTagResponse.class));
                    }
                //admin pode ver todos exercicios
                }else{
                    exercicioResponses.add(modelMapper.map(exercicioService.converteExercicioParaResponse(tagExercicio.getExercicio()), ExercicioSemTagResponse.class));
                }
            }

            return new TagComExerciciosResponse(tag.getId(), tag.getNome(), exercicioResponses);
        }else{
            return null;
        }
    }

    public TagResponse cadastrarNovaTag(TagCadastroRequest tagCadastroRequest) {
        //Todo: deixar minisculo

        Tag tag = new Tag(null, tagCadastroRequest.getNome());

        return converteTagParaResponse(salvarTagNoBanco(tag));
    }

    public TagResponse atualizarTag(Long codigo, TagAtualizacaoRequest tagAtualizacaoRequest) {
        Optional<Tag> optionalTag = buscarTagDoBancoPorCodigo(codigo);

        if (optionalTag.isEmpty()) {
            return null;
        }
        Tag tagBanco = optionalTag.get();

        Tag tag = new Tag(tagAtualizacaoRequest.getId(), tagAtualizacaoRequest.getNome());

        BeanUtils.copyProperties(tag, tagBanco, "id");

        return converteTagParaResponse(salvarTagNoBanco(tagBanco));
    }

    @Transactional
    public boolean removerTag(Long codigo) {
        Optional<Tag> optionalTag = buscarTagDoBancoPorCodigo(codigo);

        if (optionalTag.isEmpty()) {
            return false;
        }

        //remove relacionamentos
        List<TagExercicio> tagExercicios = tagExercicioRepository.findAllByTag(optionalTag.get());
        tagExercicios.forEach(tag -> removeRelacionamentoTagExercicio(tag));

        tagRepository.delete(optionalTag.get());
        return true;
    }

    private TagResponse converteTagParaResponse(Tag tag) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(tag, TagResponse.class);
    }

    protected Optional<Tag> buscarTagDoBancoPorCodigo(Long codigo) {
        return tagRepository.findById(codigo);
    }

    protected Optional<Tag> buscarTagPorNome(String nomeTag) {
        return tagRepository.findByNome(nomeTag);
    }

    protected void removeRelacionamentoTagExercicio(TagExercicio tagExercicio) {
        tagExercicioRepository.delete(tagExercicio);
    }

    protected void removeRelacionamentosTagExercicio(List<TagExercicio> tagExercicios) {
        tagExercicioRepository.deleteAll(tagExercicios);
    }

    protected Tag salvarTagNoBanco(Tag tag) {
        return tagRepository.save(tag);
    }
}
