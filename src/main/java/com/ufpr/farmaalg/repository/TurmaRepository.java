package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Instituicao;
import com.ufpr.farmaalg.model.Turma;
import com.ufpr.farmaalg.model.util.Status;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

/**
 * @author Lia Alflen
 */

public interface TurmaRepository extends JpaRepository<Turma, Long> {
    List<Turma> findAllByProfessor_Id (Long codigo);
    List<Turma> findAllByProfessor_Instituicao (Instituicao instituicao);
    Turma findByCodigoAcessoAndStatusAndProfessor_Instituicao (String codigoAcesso, Status status, Instituicao professor_instituicao);
    List<Turma> findAllByIdIn (List<Long> id);
    Turma findByCodigoAcessoAndStatusIn (String codigoAcesso, List<Status> status);
}
