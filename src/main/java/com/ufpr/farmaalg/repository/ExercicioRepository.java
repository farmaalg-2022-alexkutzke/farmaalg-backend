package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.Turma;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author Lia Alflen
 */

public interface ExercicioRepository extends JpaRepository<Exercicio, Long> {
    List<Exercicio> findAllByExercicioOriginalIdIn(List<Long> ids);
    List<Exercicio> findAllByTurma(Turma turma);
    List<Exercicio> findAllByEhPublicoAndTurma_Professor_Instituicao_IdOrTurma_Professor_Id(boolean ehPublico, Long turma_professor_instituicao_id, Long turma_professor_id);
    Optional<Exercicio> findByTurma_IdInAndId(List<Long> turma_id, Long id);

}
