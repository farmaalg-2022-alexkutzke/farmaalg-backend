package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Instituicao;
import com.ufpr.farmaalg.model.InstituicaoContato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface InstituicaoContatoRepository extends JpaRepository<InstituicaoContato, Long> {
    List<InstituicaoContato> findAllByInstituicao(Instituicao instituicao);
}