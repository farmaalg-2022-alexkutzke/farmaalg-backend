package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.Tag;
import com.ufpr.farmaalg.model.relacionamento.tag_exercicio.TagExercicio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface TagExercicioRepository extends JpaRepository<TagExercicio, Long> {

    List<TagExercicio> findAllByTag(Tag tag);
    List<TagExercicio> findAllByExercicio(Exercicio exercicio);

}
