package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.DicaExercicio;
import com.ufpr.farmaalg.model.Exercicio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface DicaExercicioRepository extends JpaRepository<DicaExercicio, Long> {
    List<DicaExercicio> findAllByExercicio(Exercicio exercicio);
    DicaExercicio findByIdAndExercicio_Turma_Professor_Id(Long id, Long exercicio_turma_professor_id);
}
