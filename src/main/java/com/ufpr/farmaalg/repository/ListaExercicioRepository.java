package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma.ListaExercicio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface ListaExercicioRepository extends JpaRepository<ListaExercicio, Long> {

    List<ListaExercicio> findByListaExercicioTurma_Id(Long id);
    List<ListaExercicio> findByExercicio_Id(Long id);

}
