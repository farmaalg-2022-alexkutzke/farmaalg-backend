package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Instituicao;
import com.ufpr.farmaalg.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    Usuario findByEmail(String email);
    List<Usuario> findAllByInstituicao(Instituicao instituicao);
}
