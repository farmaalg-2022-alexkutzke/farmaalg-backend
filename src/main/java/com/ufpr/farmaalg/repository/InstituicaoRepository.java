package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Instituicao;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lia Alflen
 */

public interface InstituicaoRepository extends JpaRepository<Instituicao, Long> {

}
