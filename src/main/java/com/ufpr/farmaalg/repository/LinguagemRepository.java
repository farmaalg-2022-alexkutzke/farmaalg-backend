package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Linguagem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lia Alflen
 */

public interface LinguagemRepository extends JpaRepository<Linguagem, Long> {

}
