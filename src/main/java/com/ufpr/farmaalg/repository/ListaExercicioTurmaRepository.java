package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.ListaExercicioTurma;
import com.ufpr.farmaalg.model.Turma;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface ListaExercicioTurmaRepository extends JpaRepository<ListaExercicioTurma, Long> {
    List<ListaExercicioTurma> findAllByTurma(Turma turma);
}
