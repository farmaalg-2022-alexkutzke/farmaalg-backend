package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.CasoTeste;
import com.ufpr.farmaalg.model.Exercicio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface CasoTesteRepository extends JpaRepository<CasoTeste, Long> {
        List<CasoTeste> findAllByExercicio(Exercicio exercicio);
}