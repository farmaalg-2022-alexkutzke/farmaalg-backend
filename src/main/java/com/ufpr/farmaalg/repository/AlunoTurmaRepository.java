package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Usuario;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurma;
import com.ufpr.farmaalg.model.Turma;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface AlunoTurmaRepository extends JpaRepository<AlunoTurma, Long> {

    List<AlunoTurma> findByTurma(Turma turma);
    List<AlunoTurma> findByAluno(Usuario aluno);
    List<AlunoTurma> findByAluno_Id(Long codigo);
    AlunoTurma findByAlunoAndTurma(Usuario aluno, Turma turma);
}
