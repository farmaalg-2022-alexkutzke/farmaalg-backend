package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.Linguagem;
import com.ufpr.farmaalg.model.Resposta;
import com.ufpr.farmaalg.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface RespostaRepository extends JpaRepository<Resposta, Long> {
    List<Resposta> findAllByExercicio(Exercicio exercicio);
    List<Resposta> findAllByAluno(Usuario aluno);
    List<Resposta> findAllByAlunoAndExercicio_Turma_Professor_Id(Usuario aluno, Long exercicio_turma_professor_id);
    List<Resposta> findAllByLinguagem(Linguagem linguagem);
    List<Resposta> findAllByExercicioAndAluno(Exercicio exercicio, Usuario aluno);
}
