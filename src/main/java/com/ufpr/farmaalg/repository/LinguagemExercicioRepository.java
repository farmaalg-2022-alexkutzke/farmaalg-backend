package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.Linguagem;
import com.ufpr.farmaalg.model.relacionamento.linguagem_exercicio.LinguagemExercicio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface LinguagemExercicioRepository extends JpaRepository<LinguagemExercicio, Long> {
    List<LinguagemExercicio> findAllByLinguagem(Linguagem linguagem);
    List<LinguagemExercicio> findAllByExercicio(Exercicio exercicio);
    LinguagemExercicio findByExercicioAndLinguagem(Exercicio exercicio, Linguagem linguagem);
}
