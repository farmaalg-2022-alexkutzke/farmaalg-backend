package com.ufpr.farmaalg.repository;

import com.ufpr.farmaalg.model.CasoTeste;
import com.ufpr.farmaalg.model.Resposta;
import com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste.RespostaCasoTeste;
import com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste.RespostaCasoTesteId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lia Alflen
 */

public interface RespostaCasoTesteRepository extends JpaRepository<RespostaCasoTeste, Long> {

    List<RespostaCasoTeste> findAllByCasoTeste(CasoTeste casoTeste);
    List<RespostaCasoTeste> findAllByResposta(Resposta resposta);
    RespostaCasoTeste findById(RespostaCasoTesteId id);

}