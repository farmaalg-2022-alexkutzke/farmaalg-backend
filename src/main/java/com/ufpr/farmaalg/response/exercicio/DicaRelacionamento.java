package com.ufpr.farmaalg.response.exercicio;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DicaRelacionamento {

    private Long id;
    private String titulo;
    private String descricao;
    private int numeroTentativas;
}
