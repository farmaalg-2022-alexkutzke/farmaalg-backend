package com.ufpr.farmaalg.response.listaexercicio;

import com.ufpr.farmaalg.request.IdRelacionamento;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExercicioResponseSimplificado {
    private Long id;
    private String titulo;
    private String descricao;
    private String enunciado;
    private boolean ehPublico;
    private IdRelacionamento exercicioOriginal;

}
