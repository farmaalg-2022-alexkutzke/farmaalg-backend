package com.ufpr.farmaalg.response.listaexercicio;

import com.ufpr.farmaalg.response.turma.TurmaResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ListaExercicioTurmaResponse {

    private Long id;
    private String titulo;
    private String descricao;
    private LocalDateTime dataEntrega;
    private TurmaResponse turma;
    private List<ExercicioResponseSimplificado> exercicios;
}
