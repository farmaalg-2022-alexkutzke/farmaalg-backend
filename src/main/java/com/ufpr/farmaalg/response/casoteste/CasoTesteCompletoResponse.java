package com.ufpr.farmaalg.response.casoteste;

import com.ufpr.farmaalg.response.exercicio.ExercicioResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CasoTesteCompletoResponse {

    private Long id;
    private String titulo;
    private String input;
    private String output;
    private ExercicioResponse exercicio;
}
