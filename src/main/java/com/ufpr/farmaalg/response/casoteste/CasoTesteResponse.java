package com.ufpr.farmaalg.response.casoteste;

import com.ufpr.farmaalg.response.listaexercicio.ExercicioResponseSimplificado;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
public class CasoTesteResponse {

    private Long id;
    private String titulo;
    private String input;
    private String output;
    private ExercicioResponseSimplificado exercicio;
}
