package com.ufpr.farmaalg.response.instituicaocontato;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
public class InstituicaoContatoResponse {

    private Long id;
    private String contato;
    private String tipo;
    private String pessoa;
    private InstituicaoResponseSemContatos instituicao;

}
