package com.ufpr.farmaalg.response.tag;

import com.ufpr.farmaalg.request.IdRelacionamento;
import com.ufpr.farmaalg.response.exercicio.DicaRelacionamento;
import com.ufpr.farmaalg.response.linguagem.LinguagemResponse;
import com.ufpr.farmaalg.response.turma.TurmaResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExercicioSemTagResponse {
    private Long id;
    private String titulo;
    private String descricao;
    private String enunciado;
    private boolean ehPublico;

    private TurmaResponse turma;
    private IdRelacionamento exercicioOriginal;
    private List<LinguagemResponse> linguagens;
    private List<TagResponse> tags;
    private List<DicaRelacionamento> dicas;

}
