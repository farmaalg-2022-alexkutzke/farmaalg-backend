package com.ufpr.farmaalg.response.tag;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TagComExerciciosResponse {

    private Long id;
    private String nome;
    private List<ExercicioSemTagResponse> exercicios;
}
