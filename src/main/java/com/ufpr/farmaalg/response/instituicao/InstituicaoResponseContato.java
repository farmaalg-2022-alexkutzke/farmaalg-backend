package com.ufpr.farmaalg.response.instituicao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InstituicaoResponseContato {
    private Long id;
    private String contato;
    private String tipo;
    private String pessoa;
}
