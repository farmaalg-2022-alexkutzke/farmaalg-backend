package com.ufpr.farmaalg.response.instituicao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InstituicaoResponse {

    private Long id;
    private String nome;
    private String cnpj;
    private String endereco;
    private String numero;
    private String bairro;
    private String cep;
    private String cidade;
    private String estado;
    private String avatar;

    private List<InstituicaoResponseContato> contatos;
}
