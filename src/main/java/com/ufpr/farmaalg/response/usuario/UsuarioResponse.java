package com.ufpr.farmaalg.response.usuario;

import com.ufpr.farmaalg.model.util.Perfil;
import com.ufpr.farmaalg.response.instituicaocontato.InstituicaoResponseSemContatos;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
public class UsuarioResponse {

    private Long id;

    @Enumerated(EnumType.STRING)
    private Perfil perfil;

    private String nome;
    private String email;
    private String idInstitucional;
    private String telefone;
    private String avatar;
    private InstituicaoResponseSemContatos instituicao;

}
