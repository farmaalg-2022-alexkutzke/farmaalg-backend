package com.ufpr.farmaalg.response.resposta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CasoTesteRespostaResponse {

    private Long id;
    private String titulo;
    private String input;
    private String output;
    private boolean estahCorreto;
    private String outputObtido;
}
