package com.ufpr.farmaalg.response.resposta;

import com.ufpr.farmaalg.response.usuario.UsuarioResponse;
import com.ufpr.farmaalg.response.linguagem.LinguagemResponse;
import com.ufpr.farmaalg.response.listaexercicio.ExercicioResponseSimplificado;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RespostaResponse {

    private Long id;
    private String codigo;
    private int numeroTentativa;
    private boolean ehFinal;
    private UsuarioResponse aluno;
    private ExercicioResponseSimplificado exercicio;
    private LinguagemResponse linguagem;
    private List<CasoTesteRespostaResponse> casosTeste;
}
