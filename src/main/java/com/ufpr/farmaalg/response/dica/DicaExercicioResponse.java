package com.ufpr.farmaalg.response.dica;

import com.ufpr.farmaalg.response.listaexercicio.ExercicioResponseSimplificado;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DicaExercicioResponse {

    private Long id;
    private ExercicioResponseSimplificado exercicio;
    private String titulo;
    private String descricao;
    private int numeroTentativas;

}
