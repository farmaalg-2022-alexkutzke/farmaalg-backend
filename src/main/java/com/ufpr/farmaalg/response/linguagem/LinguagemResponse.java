package com.ufpr.farmaalg.response.linguagem;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LinguagemResponse {

    private Long id;
    private String nome;
    private String codigoSandbox;
    private String exemplo;

}
