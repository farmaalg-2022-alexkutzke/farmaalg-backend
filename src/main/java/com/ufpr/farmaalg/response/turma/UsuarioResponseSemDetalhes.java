package com.ufpr.farmaalg.response.turma;

import com.ufpr.farmaalg.model.util.Perfil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioResponseSemDetalhes {

    private Long id;

    @Enumerated(EnumType.STRING)
    private Perfil perfil;

//    private InstituicaoResponseSemContatos instituicao;
    private String nome;
    private String email;
    private String avatar;
}
