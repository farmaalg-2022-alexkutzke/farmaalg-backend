package com.ufpr.farmaalg.response.turma;

import com.ufpr.farmaalg.response.exercicio.ExercicioSimplificadoResponse;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
public class TurmaCompletaResponse {

    private Long id;
    private String disciplina;
    private String nome;
    private String descricao;
    private String codigoAcesso;
    private String status;
    private UsuarioResponseSemDetalhes professor;
    private List<UsuarioResponseSemDetalhes> alunos;
    private List<ExercicioSimplificadoResponse> exercicios;
}
