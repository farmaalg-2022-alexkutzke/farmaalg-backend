package com.ufpr.farmaalg.response.turma;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
public class TurmaResponse {

    private Long id;
    private String disciplina;
    private String nome;
    private String descricao;
    private String codigoAcesso;
    private String status;
    private UsuarioResponseSemDetalhes professor;
}
