package com.ufpr.farmaalg.response.sandbox;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SandboxResult {
    private Float exit_code;
    private String status;
    private Float exec_time;
    private String input;
    private String output;

    @Override
    public String toString() {
        return "SandboxResult{" +
                "exit_code=" + exit_code +
                ", status='" + status + '\'' +
                ", exec_time=" + exec_time +
                ", input='" + input + '\'' +
                ", output='" + output + '\'' +
                '}';
    }
}
