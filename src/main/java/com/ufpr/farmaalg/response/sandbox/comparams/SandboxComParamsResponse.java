package com.ufpr.farmaalg.response.sandbox.comparams;

import com.ufpr.farmaalg.response.sandbox.SandboxResult;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SandboxComParamsResponse {
    private String id;
    private String status;
    private Float comp_time;
    private List<SandboxResult> result;

    @Override
    public String toString() {
        return "SandboxResponse{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", comp_time=" + comp_time +
                ", result=" + result +
                '}';
    }
}
