package com.ufpr.farmaalg.response.sandbox.erro;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SandboxComErroResponse {
    private String id;
    private String status;
    private Float comp_time;
    private String output;

    @Override
    public String toString() {
        return "SandboxResponse{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", comp_time=" + comp_time +
                ", output='" + output + '\'' +
                '}';
    }
}
