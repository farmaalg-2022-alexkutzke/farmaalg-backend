package com.ufpr.farmaalg.model.util;

/**
 * @author Lia Alflen
 */
public enum Status {
    ATIVA,
    ENCERRADA,
    ARQUIVADA,
    PAUSADA
}
