package com.ufpr.farmaalg.model.util;

/**
 * @author Lia Alflen
 */

public enum Perfil {
    ADMIN,
    PROFESSOR,
    ALUNO;
}
