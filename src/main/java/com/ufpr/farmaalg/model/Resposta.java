package com.ufpr.farmaalg.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste.RespostaCasoTeste;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "resposta")
public class Resposta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String codigo;

    @Column(name="numero_tentativa")
    private int numeroTentativa;

    @Column(name="eh_final")
    private boolean ehFinal;

    @ManyToOne
    @JoinColumn(name="id_aluno", nullable=false)
    private Usuario aluno;

    @ManyToOne
    @JoinColumn(name="id_execicio", nullable=false)
    private Exercicio exercicio;

    @ManyToOne
    @JoinColumn(name="id_linguagem", nullable=false)
    private Linguagem linguagem;

    @JsonIgnore
    @OneToMany(mappedBy = "resposta")
    private List<RespostaCasoTeste> casosTeste;

}
