package com.ufpr.farmaalg.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "instituicao_contato")
public class InstituicaoContato {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="id_instituicao", nullable=false)
    private Instituicao instituicao;
    private String contato;
    private String tipo;
    private String pessoa;

}
