package com.ufpr.farmaalg.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.relacionamento.linguagem_exercicio.LinguagemExercicio;
import com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma.ListaExercicio;
import com.ufpr.farmaalg.model.relacionamento.tag_exercicio.TagExercicio;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "exercicio")
public class Exercicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="id_turma", nullable=false)
    private Turma turma;

    @ManyToOne(optional = true)
    @JoinColumn(name="id_execicio_original")
    private Exercicio exercicioOriginal;

    private String titulo;
    private String descricao;
    private String enunciado;

    @Column(name="eh_publico")
    private boolean ehPublico;

    @JsonIgnore
    @OneToMany(mappedBy = "exercicio")
    private List<TagExercicio> tagsExercicio;

    @JsonIgnore
    @OneToMany(mappedBy = "exercicio")
    private List<LinguagemExercicio> linguagensExercicio;

    @JsonIgnore
    @OneToMany(mappedBy="exercicio")
    private List<DicaExercicio> dicas;

    public Exercicio(Turma turma, Exercicio exercicioOriginal, String titulo, String descricao, String enunciado, boolean ehPublico) {
        this.turma = turma;
        this.exercicioOriginal = exercicioOriginal;
        this.titulo = titulo;
        this.descricao = descricao;
        this.enunciado = enunciado;
        this.ehPublico = ehPublico;
    }
}
