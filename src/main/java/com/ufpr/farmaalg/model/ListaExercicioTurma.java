package com.ufpr.farmaalg.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma.ListaExercicio;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "lista_exercicio_turma")
public class ListaExercicioTurma {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String titulo;
    private String descricao;

    @Column(name="data_entrega")
    private LocalDateTime dataEntrega;

    @ManyToOne
    @JoinColumn(name="id_turma", nullable=false)
    private Turma turma;

    @JsonIgnore
    @OneToMany(mappedBy = "listaExercicioTurma")
    private List<ListaExercicio> listaExercicios;
}
