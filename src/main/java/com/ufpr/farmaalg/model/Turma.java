package com.ufpr.farmaalg.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.relacionamento.aluno_turma.AlunoTurma;
import com.ufpr.farmaalg.model.util.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "turma")
public class Turma {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="id_professor", nullable=false)
    private Usuario professor;

    @JsonIgnore
    @OneToMany(mappedBy = "turma")
    private List<AlunoTurma> alunoTurmas;

    @JsonIgnore
    @OneToMany(mappedBy="turma")
    private List<Exercicio> exercicios;

    @JsonIgnore
    @OneToMany(mappedBy="turma")
    private List<ListaExercicioTurma> listasExercicios;

    private String disciplina;
    private String nome;
    private String descricao;

    @Column(name="codigo_acesso")
    private String codigoAcesso;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Turma turma = (Turma) o;
        return id.equals(turma.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
