package com.ufpr.farmaalg.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "dica_exercicio")
public class DicaExercicio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="id_exercicio", nullable=false)
    private Exercicio exercicio;

    private String titulo;
    private String descricao;

    @Column(name="numero_tentativas")
    private int numeroTentativas;

}
