package com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Lia Alflen
 */

@Embeddable
public class RespostaCasoTesteId implements Serializable {
	@Column(name = "id_caso_teste")
	private Long idCasoTeste;

	@Column(name = "id_resposta")
	private Long idResposta;

	public RespostaCasoTesteId() {
		super();
	}

	public RespostaCasoTesteId(Long idCasoTeste, Long idResposta) {
		super();
		this.idCasoTeste = idCasoTeste;
		this.idResposta = idResposta;
	}

	public Long getIdCasoTeste() {
		return idCasoTeste;
	}

	public void setIdCasoTeste(Long idCasoTeste) {
		this.idCasoTeste = idCasoTeste;
	}

	public Long getIdResposta() {
		return idResposta;
	}

	public void setIdResposta(Long idResposta) {
		this.idResposta = idResposta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idResposta == null) ? 0 : idResposta.hashCode());
		result = prime * result + ((idCasoTeste == null) ? 0 : idCasoTeste.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RespostaCasoTesteId other = (RespostaCasoTesteId) obj;
		if (idResposta == null) {
			if (other.idResposta != null)
				return false;
		} else if (!idResposta.equals(other.idResposta))
			return false;
		if (idCasoTeste == null) {
			if (other.idCasoTeste != null)
				return false;
		} else if (!idCasoTeste.equals(other.idCasoTeste))
			return false;
		return true;
	}


}
