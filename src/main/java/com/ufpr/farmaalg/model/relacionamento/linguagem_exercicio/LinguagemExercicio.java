package com.ufpr.farmaalg.model.relacionamento.linguagem_exercicio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.Linguagem;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Entity
@Table(name = "linguagem_exercicio")
public class LinguagemExercicio {

	@JsonIgnore
	@EmbeddedId
	private LinguagemExercicioId id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_linguagem")
	@MapsId("idLinguagem")
	private Linguagem linguagem;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_exercicio")
	@MapsId("idExercicio")
	private Exercicio exercicio;


	public LinguagemExercicio() {
		super();
	}

	public LinguagemExercicio(LinguagemExercicioId id, Linguagem linguagem, Exercicio exercicio) {
		super();
		this.id = id;
		this.linguagem = linguagem;
		this.exercicio = exercicio;
	}

	public LinguagemExercicioId getId() {
		return id;
	}

	public Linguagem getLinguagem() {
		return linguagem;
	}

	public void setLinguagem(Linguagem linguagem) {
		this.linguagem = linguagem;
	}

	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exercicio == null) ? 0 : exercicio.hashCode());
		result = prime * result + ((linguagem == null) ? 0 : linguagem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinguagemExercicio other = (LinguagemExercicio) obj;
		if (exercicio == null) {
			if (other.exercicio != null)
				return false;
		} else if (!exercicio.equals(other.exercicio))
			return false;
		if (linguagem == null) {
			if (other.linguagem != null)
				return false;
		} else if (!linguagem.equals(other.linguagem))
			return false;
		return true;
	}
}
