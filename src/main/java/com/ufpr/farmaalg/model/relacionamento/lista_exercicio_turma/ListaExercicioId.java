package com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Lia Alflen
 */

@Embeddable
public class ListaExercicioId implements Serializable {
	@Column(name = "id_lista_exercicio_turma")
	private Long idListaExercicioTurma;

	@Column(name = "id_exercicio")
	private Long idExercicio;

	public ListaExercicioId() {
		super();
	}

	public ListaExercicioId(Long idListaExercicioTurma, Long idExercicio) {
		super();
		this.idListaExercicioTurma = idListaExercicioTurma;
		this.idExercicio = idExercicio;
	}

	public Long getIdListaExercicioTurma() {
		return idListaExercicioTurma;
	}

	public void setIdListaExercicioTurma(Long idListaExercicioTurma) {
		this.idListaExercicioTurma = idListaExercicioTurma;
	}

	public Long getIdExercicio() {
		return idExercicio;
	}

	public void setIdExercicio(Long idExercicio) {
		this.idExercicio = idExercicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idExercicio == null) ? 0 : idExercicio.hashCode());
		result = prime * result + ((idListaExercicioTurma == null) ? 0 : idListaExercicioTurma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListaExercicioId other = (ListaExercicioId) obj;
		if (idExercicio == null) {
			if (other.idExercicio != null)
				return false;
		} else if (!idExercicio.equals(other.idExercicio))
			return false;
		if (idListaExercicioTurma == null) {
			if (other.idListaExercicioTurma != null)
				return false;
		} else if (!idListaExercicioTurma.equals(other.idListaExercicioTurma))
			return false;
		return true;
	}


}
