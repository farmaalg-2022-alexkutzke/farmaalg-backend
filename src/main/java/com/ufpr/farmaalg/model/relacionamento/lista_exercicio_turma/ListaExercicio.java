package com.ufpr.farmaalg.model.relacionamento.lista_exercicio_turma;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.ListaExercicioTurma;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Entity
@Table(name = "lista_exercicio")
public class ListaExercicio {

	@JsonIgnore
	@EmbeddedId
	private ListaExercicioId id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_lista_exercicio_turma")
	@MapsId("idListaExercicioTurma")
	private ListaExercicioTurma listaExercicioTurma;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_exercicio")
	@MapsId("idExercicio")
	private Exercicio exercicio;


	public ListaExercicio() {
		super();
	}

	public ListaExercicio(ListaExercicioId id, ListaExercicioTurma listaExercicioTurma, Exercicio exercicio) {
		super();
		this.id = id;
		this.listaExercicioTurma = listaExercicioTurma;
		this.exercicio = exercicio;
	}

	public ListaExercicioId getId() {
		return id;
	}

	public ListaExercicioTurma getListaExercicioTurma() {
		return listaExercicioTurma;
	}

	public void setListaExercicioTurma(ListaExercicioTurma listaExercicioTurma) {
		this.listaExercicioTurma = listaExercicioTurma;
	}

	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exercicio == null) ? 0 : exercicio.hashCode());
		result = prime * result + ((listaExercicioTurma == null) ? 0 : listaExercicioTurma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListaExercicio other = (ListaExercicio) obj;
		if (exercicio == null) {
			if (other.exercicio != null)
				return false;
		} else if (!exercicio.equals(other.exercicio))
			return false;
		if (listaExercicioTurma == null) {
			if (other.listaExercicioTurma != null)
				return false;
		} else if (!listaExercicioTurma.equals(other.listaExercicioTurma))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ListaExercicio{" +
				"id=" + id +
				", listaExercicioTurma=" + listaExercicioTurma +
				", exercicio=" + exercicio +
				'}';
	}
}
