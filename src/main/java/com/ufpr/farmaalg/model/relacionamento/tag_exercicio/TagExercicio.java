package com.ufpr.farmaalg.model.relacionamento.tag_exercicio;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.Exercicio;
import com.ufpr.farmaalg.model.Tag;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Entity
@Table(name = "tag_exercicio")
public class TagExercicio {

	@JsonIgnore
	@EmbeddedId
	private TagExercicioId id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tag")
	@MapsId("idTag")
	private Tag tag;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_exercicio")
	@MapsId("idExercicio")
	private Exercicio exercicio;


	public TagExercicio() {
		super();
	}

	public TagExercicio(TagExercicioId id, Tag tag, Exercicio exercicio) {
		super();
		this.id = id;
		this.tag = tag;
		this.exercicio = exercicio;
	}

	public TagExercicioId getId() {
		return id;
	}

	public Tag getTag() {
		return tag;
	}

	public void setTag(Tag tag) {
		this.tag = tag;
	}

	public Exercicio getExercicio() {
		return exercicio;
	}

	public void setExercicio(Exercicio exercicio) {
		this.exercicio = exercicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exercicio == null) ? 0 : exercicio.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagExercicio other = (TagExercicio) obj;
		if (exercicio == null) {
			if (other.exercicio != null)
				return false;
		} else if (!exercicio.equals(other.exercicio))
			return false;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}
}
