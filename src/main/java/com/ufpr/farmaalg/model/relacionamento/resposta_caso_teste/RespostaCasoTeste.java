package com.ufpr.farmaalg.model.relacionamento.resposta_caso_teste;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.CasoTeste;
import com.ufpr.farmaalg.model.Resposta;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Entity
@Table(name = "resposta_caso_teste")
public class RespostaCasoTeste {

	@JsonIgnore
	@EmbeddedId
	private RespostaCasoTesteId id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_caso_teste")
	@MapsId("idCasoTeste")
	private CasoTeste casoTeste;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_resposta")
	@MapsId("idResposta")
	private Resposta resposta;

	@Column(name="estah_correto")
	private boolean estahCorreto;

	@Column(name="output_obtido")
	private String outputObtido;

	public RespostaCasoTeste() {
		super();
	}

	public RespostaCasoTeste(RespostaCasoTesteId id, CasoTeste casoTeste, Resposta resposta, boolean estahCorreto, String outputObtido) {
		super();
		this.id = id;
		this.casoTeste = casoTeste;
		this.resposta = resposta;
		this.estahCorreto = estahCorreto;
		this.outputObtido = outputObtido;
	}

	public RespostaCasoTesteId getId() {
		return id;
	}

	public CasoTeste getCasoTeste() {
		return casoTeste;
	}

	public void setCasoTeste(CasoTeste casoTeste) {
		this.casoTeste = casoTeste;
	}

	public Resposta getResposta() {
		return resposta;
	}

	public void setResposta(Resposta resposta) {
		this.resposta = resposta;
	}

	public boolean isEstahCorreto() {
		return estahCorreto;
	}

	public void setEstahCorreto(boolean estahCorreto) {
		this.estahCorreto = estahCorreto;
	}

	public String getOutputObtido() {
		return outputObtido;
	}

	public void setOutputObtido(String outputObtido) {
		this.outputObtido = outputObtido;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((resposta == null) ? 0 : resposta.hashCode());
		result = prime * result + ((casoTeste == null) ? 0 : casoTeste.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RespostaCasoTeste other = (RespostaCasoTeste) obj;
		if (resposta == null) {
			if (other.resposta != null)
				return false;
		} else if (!resposta.equals(other.resposta))
			return false;
		if (casoTeste == null) {
			if (other.casoTeste != null)
				return false;
		} else if (!casoTeste.equals(other.casoTeste))
			return false;
		return true;
	}
}
