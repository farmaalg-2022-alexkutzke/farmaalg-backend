package com.ufpr.farmaalg.model.relacionamento.linguagem_exercicio;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Lia Alflen
 */

@Embeddable
public class LinguagemExercicioId implements Serializable {
	@Column(name = "id_linguagem")
	private Long idLinguagem;

	@Column(name = "id_exercicio")
	private Long idExercicio;

	public LinguagemExercicioId() {
		super();
	}

	public LinguagemExercicioId(Long idLinguagem, Long idExercicio) {
		super();
		this.idLinguagem = idLinguagem;
		this.idExercicio = idExercicio;
	}

	public Long getIdLinguagem() {
		return idLinguagem;
	}

	public void setIdLinguagem(Long idLinguagem) {
		this.idLinguagem = idLinguagem;
	}

	public Long getIdExercicio() {
		return idExercicio;
	}

	public void setIdExercicio(Long idExercicio) {
		this.idExercicio = idExercicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idExercicio == null) ? 0 : idExercicio.hashCode());
		result = prime * result + ((idLinguagem == null) ? 0 : idLinguagem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinguagemExercicioId other = (LinguagemExercicioId) obj;
		if (idExercicio == null) {
			if (other.idExercicio != null)
				return false;
		} else if (!idExercicio.equals(other.idExercicio))
			return false;
		if (idLinguagem == null) {
			if (other.idLinguagem != null)
				return false;
		} else if (!idLinguagem.equals(other.idLinguagem))
			return false;
		return true;
	}


}
