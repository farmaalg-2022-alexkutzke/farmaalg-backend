package com.ufpr.farmaalg.model.relacionamento.aluno_turma;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.Turma;
import com.ufpr.farmaalg.model.Usuario;

import javax.persistence.*;

/**
 * @author Lia Alflen
 */

@Entity
@Table(name = "aluno_turma")
public class AlunoTurma {

	@JsonIgnore
	@EmbeddedId
	private AlunoTurmaId id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_turma")
	@MapsId("idTurma")
	private Turma turma;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_aluno")
	@MapsId("idAluno")
	private Usuario aluno;

	public AlunoTurma() {
		super();
	}

	public AlunoTurma(AlunoTurmaId id, Turma turma, Usuario aluno) {
		super();
		this.id = id;
		this.turma = turma;
		this.aluno = aluno;
	}

	public AlunoTurmaId getId() {
		return id;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Usuario getAluno() {
		return aluno;
	}

	public void setAluno(Usuario aluno) {
		this.aluno = aluno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aluno == null) ? 0 : aluno.hashCode());
		result = prime * result + ((turma == null) ? 0 : turma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoTurma other = (AlunoTurma) obj;
		if (aluno == null) {
			if (other.aluno != null)
				return false;
		} else if (!aluno.equals(other.aluno))
			return false;
		if (turma == null) {
			if (other.turma != null)
				return false;
		} else if (!turma.equals(other.turma))
			return false;
		return true;
	}
}
