package com.ufpr.farmaalg.model.relacionamento.aluno_turma;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Lia Alflen
 */

@Embeddable
public class AlunoTurmaId implements Serializable {
	@Column(name = "id_turma")
	private Long idTurma;

	@Column(name = "id_aluno")
	private Long idAluno;

	public AlunoTurmaId() {
		super();
	}

	public AlunoTurmaId(Long turmaId, Long alunoId) {
		super();
		this.idTurma = turmaId;
		this.idAluno = alunoId;
	}

	public Long getIdTurma() {
		return idTurma;
	}

	public void setIdTurma(Long idTurma) {
		this.idTurma = idTurma;
	}

	public Long getIdAluno() {
		return idAluno;
	}

	public void setIdAluno(Long idAluno) {
		this.idAluno = idAluno;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idAluno == null) ? 0 : idAluno.hashCode());
		result = prime * result + ((idTurma == null) ? 0 : idTurma.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AlunoTurmaId other = (AlunoTurmaId) obj;
		if (idAluno == null) {
			if (other.idAluno != null)
				return false;
		} else if (!idAluno.equals(other.idAluno))
			return false;
		if (idTurma == null) {
			if (other.idTurma != null)
				return false;
		} else if (!idTurma.equals(other.idTurma))
			return false;
		return true;
	}


}
