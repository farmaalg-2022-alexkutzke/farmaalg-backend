package com.ufpr.farmaalg.model.relacionamento.tag_exercicio;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Lia Alflen
 */

@Embeddable
public class TagExercicioId implements Serializable {
	@Column(name = "id_tag")
	private Long idTag;

	@Column(name = "id_exercicio")
	private Long idExercicio;

	public TagExercicioId() {
		super();
	}

	public TagExercicioId(Long idTag, Long idExercicio) {
		super();
		this.idTag = idTag;
		this.idExercicio = idExercicio;
	}

	public Long getIdTag() {
		return idTag;
	}

	public void setIdTag(Long idTag) {
		this.idTag = idTag;
	}

	public Long getIdExercicio() {
		return idExercicio;
	}

	public void setIdExercicio(Long idExercicio) {
		this.idExercicio = idExercicio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idExercicio == null) ? 0 : idExercicio.hashCode());
		result = prime * result + ((idTag == null) ? 0 : idTag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagExercicioId other = (TagExercicioId) obj;
		if (idExercicio == null) {
			if (other.idExercicio != null)
				return false;
		} else if (!idExercicio.equals(other.idExercicio))
			return false;
		if (idTag == null) {
			if (other.idTag != null)
				return false;
		} else if (!idTag.equals(other.idTag))
			return false;
		return true;
	}


}
