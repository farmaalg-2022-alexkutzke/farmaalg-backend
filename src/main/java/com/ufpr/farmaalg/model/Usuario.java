package com.ufpr.farmaalg.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ufpr.farmaalg.model.util.Perfil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * @author Lia Alflen
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Perfil perfil;

    @ManyToOne(optional = true)
    @JoinColumn(name="id_instituicao")
    private Instituicao instituicao;

    @JsonIgnore
    @OneToMany(mappedBy="professor")
    private List<Turma> turmasProfessor;

    private String nome;
    private String email;

    @Column(name="id_institucional")
    private String idInstitucional;

    private String telefone;
    private String avatar;
    private String senha;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id.equals(usuario.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
