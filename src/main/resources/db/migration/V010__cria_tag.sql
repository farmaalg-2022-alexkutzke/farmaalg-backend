CREATE TABLE tag (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(50) NOT NULL
);

INSERT INTO tag(nome) VALUES
    ('algorítmo'),
    ('array'),
    ('árvore binária'),
    ('árvore'),
    ('fatorial'),
    ('fila'),
    ('lista encadeada'),
    ('lista'),
    ('lógica'),
    ('ordenação'),
    ('orientação a objetos'),
    ('passagem por referência'),
    ('pesquisa'),
    ('pesquisa'),
    ('pilha'),
    ('ponteiro'),
    ('recursividade'),
    ('sequência fibonacci'),
    ('sort'),
    ('vetor');
