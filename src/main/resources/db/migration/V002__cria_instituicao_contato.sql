CREATE TABLE instituicao_contato(
    id SERIAL PRIMARY KEY,
    id_instituicao INTEGER NOT NULL,
    contato VARCHAR(100) NOT NULL,
    tipo VARCHAR(14) NOT NULL,
    pessoa VARCHAR(50) NOT NULL,
    FOREIGN KEY (id_instituicao) REFERENCES instituicao (id)
);

INSERT INTO instituicao_contato(contato, tipo, id_instituicao, pessoa) VALUES
    ('alexander@ufpr.br', 'E-mail', 1, 'Alex (admin)');
