CREATE TABLE caso_teste(
    id SERIAL PRIMARY KEY,
    id_execicio INTEGER NOT NULL,
    titulo VARCHAR(100) NOT NULL,
    input VARCHAR(200),
    output TEXT NOT NULL,
    FOREIGN KEY (id_execicio) REFERENCES exercicio (id)
);
