CREATE TABLE instituicao(
    id SERIAL PRIMARY KEY,
    nome VARCHAR(100) NOT NULL,
    cnpj VARCHAR(14) NOT NULL,
    endereco VARCHAR(50) NOT NULL,
    numero VARCHAR(10) NOT NULL,
    bairro VARCHAR(50) NOT NULL,
    cep VARCHAR(15) NOT NULL,
    cidade VARCHAR(50) NOT NULL,
    estado VARCHAR(2) NOT NULL
);

INSERT INTO instituicao(nome, cnpj, endereco, numero, bairro, cep, cidade, estado) VALUES
    ('Universidade Federal do Paraná', '75095679000149', 'Rua XV de Novembro', '1299', 'Centro','80060000', 'Curitiba', 'PR');
