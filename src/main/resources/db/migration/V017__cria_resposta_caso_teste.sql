CREATE TABLE resposta_caso_teste(
    id_caso_teste INTEGER NOT NULL,
    id_resposta INTEGER NOT NULL,
    estah_correto BOOL NOT NULL,
    output_obtido TEXT,
    CONSTRAINT PK_RESPOSTA_CASO_TESTE PRIMARY KEY ( id_caso_teste, id_resposta ),
    FOREIGN KEY (id_caso_teste) REFERENCES caso_teste (id),
    FOREIGN KEY (id_resposta) REFERENCES resposta (id)
);
