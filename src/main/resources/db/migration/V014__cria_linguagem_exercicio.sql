CREATE TABLE linguagem_exercicio(
    id_linguagem INTEGER NOT NULL,
    id_exercicio INTEGER NOT NULL,
    CONSTRAINT PK_LINGUAGEM_EXERCICIO PRIMARY KEY ( id_linguagem, id_exercicio ),
    FOREIGN KEY (id_linguagem) REFERENCES linguagem (id),
    FOREIGN KEY (id_exercicio) REFERENCES exercicio (id)
);
