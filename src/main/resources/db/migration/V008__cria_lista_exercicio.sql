CREATE TABLE lista_exercicio(
    id_lista_exercicio_turma INTEGER NOT NULL,
    id_exercicio INTEGER NOT NULL,
    CONSTRAINT PK_LISTA_EXERCICIO PRIMARY KEY ( id_lista_exercicio_turma, id_exercicio ),
    FOREIGN KEY (id_lista_exercicio_turma) REFERENCES lista_exercicio_turma (id),
    FOREIGN KEY (id_exercicio) REFERENCES exercicio (id)
);
