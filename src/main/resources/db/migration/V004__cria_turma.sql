CREATE TABLE turma(
    id SERIAL PRIMARY KEY,
    id_professor INTEGER NOT NULL,
    disciplina VARCHAR(50) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    descricao VARCHAR(255),
    codigo_acesso VARCHAR(20) NOT NULL,
    status VARCHAR(20) NOT NULL,
    FOREIGN KEY (id_professor) REFERENCES usuario (id)
);
