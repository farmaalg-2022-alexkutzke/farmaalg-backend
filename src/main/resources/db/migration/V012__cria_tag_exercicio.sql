CREATE TABLE tag_exercicio(
    id_tag INTEGER NOT NULL,
    id_exercicio INTEGER NOT NULL,
    CONSTRAINT PK_TAG_EXERCICIO PRIMARY KEY ( id_tag, id_exercicio ),
    FOREIGN KEY (id_tag) REFERENCES tag (id),
    FOREIGN KEY (id_exercicio) REFERENCES exercicio (id)
);
