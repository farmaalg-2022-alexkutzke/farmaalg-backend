CREATE TABLE exercicio(
    id SERIAL PRIMARY KEY,
    id_turma INTEGER NOT NULL,
    id_execicio_original INTEGER,
    titulo VARCHAR(100) NOT NULL,
    descricao VARCHAR(255) NOT NULL,
    enunciado TEXT NOT NULL,
    eh_publico BOOL NOT NULL,
    FOREIGN KEY (id_turma) REFERENCES turma (id),
    FOREIGN KEY (id_execicio_original) REFERENCES exercicio (id)
);
