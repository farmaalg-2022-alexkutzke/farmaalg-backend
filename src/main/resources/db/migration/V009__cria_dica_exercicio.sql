CREATE TABLE dica_exercicio(
    id SERIAL PRIMARY KEY,
    id_exercicio INTEGER NOT NULL,
    titulo VARCHAR(50) NOT NULL,
    descricao VARCHAR(255) NOT NULL,
    numero_tentativas INTEGER NOT NULL,
    FOREIGN KEY (id_exercicio) REFERENCES exercicio (id)
);
