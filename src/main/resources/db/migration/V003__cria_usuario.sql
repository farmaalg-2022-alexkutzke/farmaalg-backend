CREATE TABLE usuario(
    id SERIAL PRIMARY KEY,
    perfil VARCHAR(10) NOT NULL,
    id_instituicao INTEGER,
    nome VARCHAR(100) NOT NULL,
    email VARCHAR(50) UNIQUE NOT NULL,
    id_institucional VARCHAR(25),
    telefone VARCHAR(15),
    avatar VARCHAR(255) NOT NULL,
    senha VARCHAR(100) NOT NULL,
    FOREIGN KEY (id_instituicao) REFERENCES instituicao (id)
);

INSERT INTO usuario(nome, perfil, email, id_institucional, telefone, avatar, senha, id_instituicao) VALUES
    ('Alex Kutzke', 'ADMIN', 'alexander@ufpr.br', NULL, NULL, 'https://avatars.dicebear.com/api/identicon/01.svg?b=%23ffffff', '$2a$10$FAXFArjQa6YmJkF2jukjiOj59m1BClCFB.1c1s6PMHSzuCN5QPjPm', NULL),
    ('Desenvolvedor', 'ADMIN', 'dev@ufpr.br', NULL, NULL, 'https://avatars.dicebear.com/api/identicon/02.svg?b=%23ffffff', '$2a$10$FAXFArjQa6YmJkF2jukjiOj59m1BClCFB.1c1s6PMHSzuCN5QPjPm', NULL);
