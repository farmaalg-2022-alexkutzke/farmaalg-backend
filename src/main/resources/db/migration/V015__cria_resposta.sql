CREATE TABLE resposta (
    id SERIAL PRIMARY KEY,
    id_aluno INTEGER NOT NULL,
    id_execicio INTEGER NOT NULL,
    id_linguagem INTEGER NOT NULL,
    codigo TEXT NOT NULL,
    numero_tentativa INTEGER NOT NULL,
    eh_final BOOL NOT NULL,
    FOREIGN KEY (id_aluno) REFERENCES usuario (id),
    FOREIGN KEY (id_execicio) REFERENCES exercicio (id),
    FOREIGN KEY (id_linguagem) REFERENCES linguagem (id)
);
