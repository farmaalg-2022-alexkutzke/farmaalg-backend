CREATE TABLE linguagem(
    id SERIAL PRIMARY KEY,
    nome VARCHAR(50) NOT NULL,
    codigo_sandbox VARCHAR(50) NOT NULL,
    exemplo TEXT
);

INSERT INTO linguagem(nome, codigo_sandbox, exemplo) VALUES
    (
        'C',
        'c',
        '#include <stdlib.h>
#include <stdio.h>

int main()
{
  char nome[255];
  scanf("%s", nome);
  printf("hello, %s!\\n", nome);

  return 0;
}'
    ),
    (
        'Java 11',
        'java',
        '// IMPORTANTE: não defina nenhum pacote (package)
import java.util.Scanner;

class FarmaAlg {
  public static void main(String[] args) throws Exception {
    try (Scanner scanner = new Scanner(System.in)) {
      String nome = scanner.next();
      System.out.println("hello, " + nome + "!");
    } catch (Exception ex) {
      throw ex;
    }
  }
}'
    ),
    (
        'JavaScript (node)',
        'js',
        '
function createInputStream() {
  function openInputInterface() {
    return new Promise((resolve) => {
      let inputString = ""
      process.stdin.on("data", (input) => inputString += input)
      process.stdin.on("end", () => resolve(inputString.split("\\n")))
    })
  }

  const inputsPromise = openInputInterface()
  let currentLine = 0

  return async () => {
    const inputs = await inputsPromise

    if (currentLine < inputs.length) {
      return inputs[currentLine++]
    }
  }
}

async function main() {
  const readline = createInputStream()
  const nome = await readline()
  console.log("hello, " + nome + "!")

  process.exit(0)
}

main()
'
    ),
    (
        'Pascal',
        'pascal',
        'program FarmaAlg;

var
  nome : string;

begin
  readln(nome);
  writeln("hello, ", nome, "!");
end.
'
    ),
    (
        'PHP',
        'php',
        '<?php

function main() {
  $nome = readline();
  echo "hello, $nome!\\n";
}

main();
'
    ),
    (
        'Python 3',
        'python',
        '
def main():
  nome = input()
  print("hello, {}!".format(nome))

main()
'
    );
