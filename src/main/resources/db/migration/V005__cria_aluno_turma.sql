CREATE TABLE aluno_turma(
    id_turma INTEGER NOT NULL,
    id_aluno INTEGER NOT NULL,
    CONSTRAINT PK_ALUNO_TURMA PRIMARY KEY ( id_turma, id_aluno ),
    FOREIGN KEY (id_turma) REFERENCES turma (id),
    FOREIGN KEY (id_aluno) REFERENCES usuario (id)
);
