CREATE TABLE lista_exercicio_turma(
    id SERIAL PRIMARY KEY,
    id_turma INTEGER NOT NULL,
    titulo VARCHAR(100) NOT NULL,
    descricao VARCHAR(100) NOT NULL,
    data_entrega TIMESTAMP,
    FOREIGN KEY (id_turma) REFERENCES turma (id)
);
